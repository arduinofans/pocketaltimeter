﻿#ifndef EEPROM_HELPER_H
#define EEPROM_HELPER_H

#include <EEPROM.h>

#define EEPROM_VALUE_PROGRAM_MARKER                         ((byte) 0x41)
#define EEPROM_INDEX_PROGRAM_MARKER                         ((byte) 0)
#define EEPROM_INDEX_CONTRAST                               ((byte) 1)
#define EEPROM_INDEX_TURN_OFF_DISPLAY_AFTER_MIN             ((byte) 2)
#define EEPROM_INDEX_READ_SENSOR_PERIOD_S                   ((byte) 3)
#define EEPROM_INDEX_CAN_WRITE_TO_EEPROM                    ((byte) 4)
#define EEPROM_INDEX_INTERPOLATE_ALTITUTE                   ((byte) 5)
#define EEPROM_INDEX_CALIBRATE_ALTITUTE_M                   ((byte) 6)  // 2 bytes
#define EEPROM_INDEX_BACKLIGHT                              ((byte) 8)


#define EEPROM_INDEX_STATISTICS_DATA                        ((int) 600) // was 500 till may 2018

class GlobalSettings
{
public:
    byte settingsValue_Contrast;                // values are defined in constants.h
    byte settingsValue_TurnOffDisplayAfterMin;  // values are defined in constants.h
    byte settingsValue_ReadSensorPeriodS;       // values are defined in constants.h
    bool canWriteToEEPROM;                      // a flag if we can write to the EEPROM
    bool settingsValue_InterpolateAltitute;     // a flag if we have to imterpolate
    int settingsValue_CalibrateAltituteMeters;  // the +/- that we have to calibrate the altitute
    byte settingsValue_backlight;               // the backlight percentage
};

void writeSetting(int eepromIndex, byte eepromValue);
void writeSetting2Bytes(int eepromIndex, int eepromValue);
byte readSetting(int eepromIndex);

void initEEPROMData();
void resetEEPROMData();

bool checkEEPROMIsInitialized();


void saveStatisticsToEEPROM();
   
void readStatisticsFromEEPROM();

#endif // EEPROM_HELPER_H
