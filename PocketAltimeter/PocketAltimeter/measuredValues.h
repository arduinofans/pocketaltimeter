#ifndef MEASURED_VALUES_H
#define MEASURED_VALUES_H

#include <EEPROM.h>
#include "libraries\bme280_Drotek\BME280.h"


#define FLAG_INDEX_TEMPERATURE                  ((byte)0)
#define FLAG_INDEX_HUMIDITY                     ((byte)1)
#define FLAG_INDEX_PRESSURE                     ((byte)2)
#define FLAG_INDEX_ALTITUTE                     ((byte)3)
#define FLAG_INDEX_LAST_REGISTERED_ALTITUTE     ((byte)4)
#define FLAG_INDEX_TEMP_DENIVELATION_UP         ((byte)5)
#define FLAG_INDEX_TEMP_DENIVELATION_DOWN       ((byte)6)
#define FLAG_INDEX_TOTAL_DENIVELATION_UP        ((byte)7)
#define FLAG_INDEX_TOTAL_DENIVELATION_DOWN      ((byte)8)
#define FLAG_INDEX_LAP_DENIVELATION_UP          ((byte)9)
#define FLAG_INDEX_LAP_DENIVELATION_DOWN        ((byte)10)
#define FLAG_INDEX_MIN_TEMPERATURE              ((byte)11)
#define FLAG_INDEX_MAX_TEMPERATURE              ((byte)12)
#define FLAG_INDEX_MIN_HUMIDITY                 ((byte)13)
#define FLAG_INDEX_MAX_HUMIDITY                 ((byte)14)
#define FLAG_INDEX_MIN_PRESSURE                 ((byte)15)
#define FLAG_INDEX_MAX_PRESSURE                 ((byte)16)
#define FLAG_INDEX_MIN_ALTITUTE                 ((byte)17)
#define FLAG_INDEX_MAX_ALTITUTE                 ((byte)18)

#define INITIAL_DATA                            ((int)0xFFFF)

#define DENIVELATION_NEEDED_TO_MARK_DM          ((long)20)  // in decimeters, 1 = 10sm, 10 = 1m; default - 20
#define DENIVELATION_NEEDED_TO_SAVE_TO_EEPROM_DM ((long)500) // in decimeters; default - 500dm=50m

#define SAVE_STATISTICS_2_EEPROM_MIN            ((long) 15)  // save to EEPROM every 15 mins the data

#define LAP_LOGGER_COUNT                        ((int) 7)


struct BME280Data
{    
    int temperature;
    int humidity;
    int pressure;
    long altitute;
};

struct LapStatistics
{
    long lapTotalDenivelationUp;
    long lapTotalDenivelationDown;
    int lapMinAltitute;
    int lapMaxAltitute;
};

class MeasuredValues //StatisticData
{
public:
    long lastTimeTemperatureIsMeasuredMs;   // not in EEPROM
    long lastTimeSavedtoEEPROM;             // TODO: it should not be together with the data; try to make it int, not long

    // store to EEPROM only when the difference becomes bigger with actual values
    long sumTotalDenivelationWhenSaving; // todo - this should not be stored to eeprom...

    // EEPROM values - begin
    BME280Data prevSensorData;        // keep track of the previous two measurements to check if some invalid measurements are detected
//    BME280Data prevPrevSensorData;    // keep track of the previous two measurements to check if some invalid measurements are detected

    long eepromWriteCounter; // count the number of writes we have performed

    int temperature;
    int humidity;
    int pressure;
    long altitute;
    long altituteRaw; // todo: this is just for testing

    bool upDownFlag;
    long lastRegisteredAltitute;

    int tempDenivelationUp;
    int tempDenivelationDown;
    int maxTempDenivelationUp;
    int maxTempDenivelationDown;
    long totalDenivelationUp;
    long totalDenivelationDown;
    int minTemperature;
    int maxTemperature;
    int minHumidity;
    int maxHumidity;
    int minPressure;
    int maxPressure;
    int minAltitute;
    long maxAltitute;

    byte isChangedFlags[3]; // store flags for all monitoring values if they are changed

    // lap statistics
    char currentLapIndex;    // the index of the current lap; from 0 to LAP_LOGGER_COUNT - 1; = -1 when none; = LAP_LOGGER_COUNT - 1 when all
    LapStatistics lapStatistics[LAP_LOGGER_COUNT];


    // EEPROM values - end

public:
    bool isTimeToReadSensorData();
    bool isTimeToSaveSensorData();

    bool evaluateMeasurements(int newTemperatureInt, int newPressureInt, int newHumidityInt);

    void interpolateAltitute(int& newPressure, long& newAltitute);
    void setNewValues(float& newTemperature, float& newPressure, float& newAltitute, float& newHumidity);

    void resetData();

    void startLap();

    void setFlagBit(byte flagIndex, bool flagValue)
    {
        byte byteIndex = flagIndex / 8;
        byte bitIndex = flagIndex % 8;
        byte clearingByte = 1 << bitIndex;
        byte adjustedByte = (flagValue ? 1 : 0) << bitIndex;
        isChangedFlags[byteIndex] &= ~clearingByte;       // clear the bit
        isChangedFlags[byteIndex] |= adjustedByte;        // set the bit
    }
    void clearFlagBit(byte flagIndex)
    {
        setFlagBit(flagIndex, false);
    }
    bool getFlagBit(byte flagIndex)
    {
        byte byteIndex = flagIndex / 8;
        byte bitIndex = flagIndex % 8;
        bool bitValue = (isChangedFlags[byteIndex] >> bitIndex) & 1;
        return bitValue;
    }
};

// here is how we can use it
bool GetBMECurrentData(float& temperature, float& pressure, float& altitute, float& humidity);

bool ReadSensorData();


#endif // MEASURED_VALUES_H
