#ifndef CONSTANTS_H
#define CONSTANTS_H

#include "Arduino.h"

////////////////////////////////////////////////////////////////
// PIN constants
////////////////////////////////////////////////////////////////
// PINs buttons
const byte PIN_button1 = 2;
const byte PIN_button4 = 3;
const byte PIN_button3 = 4;
//const byte PIN_led = 13;
//const byte PIN_led_big = 5;

// display constants
#define PIN_SCE     12  // LCD CS  .... Pin 3
#define PIN_RESET   10  // LCD RST .... Pin 1
#define PIN_DC      8  // LCD Dat/Com. Pin 5
#define PIN_SDIN    11 //4  // LCD SPIDat . Pin 6
#define PIN_SCLK    13 //3  // LCD SPIClk . Pin 4
#define PIN_BLIGHT  9  //Back light

// BME280 sensor:
// Vin (Voltage In)         ->  3.3V, 5V
// Gnd (Ground)             ->  Gnd
// SDA (Serial Data)        ->  A4
// SCK, SCL (Serial Clock)  ->  A5 

////////////////////////////////////////////////////////////////
// Button press constants
////////////////////////////////////////////////////////////////
#define LOND_PRESS_TIME                             ((int) 700)     /* time in ms to wait to detect long click */
#define TIME_BEFORE_GOING_TO_SLEEP                  ((int) 8000)    /* time in ms to wait before going to sleep when no button is pressed */


////////////////////////////////////////////////////////////////
// State Machine constants
////////////////////////////////////////////////////////////////
//#define INTERVAL_TO_MEASURE_TEMPERATURE_S         ((int) 10)

#define GOTO_DEEP_SLEEP_AFTER_IDLE_S                ((int)30)
//#define TURN_OFF_SCREEN_AFTER_S                   ((int) 240)

#define SETTINGS_SCREEN_LEAVE_TIMEOUT_S             ((int) 10)
#define SUMMARY_SCREEN_LEAVE_TIMEOUT_S              ((int) 30)

#define NORMAL_STATE_SLEEP_TIME_MS                  ((int)5)
//#define NORMAL_STATE_DEEP_SLEEP_TIME_S            ((int) 8) /* should be devided by 8 */
#define TRACKING_STATE_SLEEP_TIME_MS                ((int)5)
//#define TRACKING_STATE_DEEP_SLEEP_TIME_S          ((int)16) /* should be devided by 8 */
#define SETTINGS_STATE_SLEEP_TIME_MS                ((int)6)
//#define SETTINGS_STATE_DEEP_SLEEP_TIME_S          ((int) 0) /* should be devided by 8 */
#define SUMMARY_STATE_SLEEP_TIME_MS                 ((int)6)
//#define SUMMARY_STATE_DEEP_SLEEP_TIME_S           ((int) 0) /* should be devided by 8 */
#define LAP_SUMMARY_STATE_SLEEP_TIME_MS             ((int)10)
#define MESSAGE_BOX_STATE_SLEEP_TIME_MS             ((int)10)
//#define MESSAGE_BOX_STATE_DEEP_SLEEP_TIME_S       ((int) 0) /* should be devided by 8 */

#define ARRAY_SIZE(array)                           (sizeof(array)/sizeof(array[0]))



#define DEFAULT_SETTING_VALUE_CONTRAST                      ((byte) 60)
#define DEFAULT_SETTING_VALUE_TURN_OFF_DISPLAY_AFTER_MIN    ((byte) 5)
#define DEFAULT_SETTING_VALUE_READ_SENSOR_PERIOD_S          ((byte) 10)
#define DEFAULT_SETTING_CAN_WRITE_TO_EEPROM                 ((bool) true)
#define DEFAULT_SETTING_INTERPOLATE_ALTITUTE                ((bool) false)
#define DEFAULT_SETTING_CALIBRATE_ALTITUTE_M                ((char) 0)
#define DEFAULT_SETTING_BACKLIGHT                           ((byte) 0) // no light
#define DEFAULT_VALUE_BACKLIGHT_FORCE_ACTIVATION            ((byte) 60) // when we force to see backlight


// used below in a local function
#define SETTINGS_ARRAY_CONTRAST                     {50, 55, 58, 60, 62, 65, 70, 75};
#define SETTINGS_ARRAY_TURN_OFF_DISPLAY_AFTER_MIN   {1, 5, 10, 20, 40};
#define SETTINGS_ARRAY_READ_SENSOR_PERIOD_S         {1, 3, 5, 10}; // {1, 3, 5, 10, 30, 60, 240};
#define SETTINGS_ARRAY_READ_SENSOR_PERIOD_MULTIPLYER ((int) 1)
#define SETTINGS_ARRAY_INTERPOLATE_ALTITUTE         {0, 1};
#define SETTINGS_ARRAY_BACKLIGHT_PERCENTAGE         {0, 2, 5, 10, 20, 30, 40, 50, 70, 90};


#define VOLTAGE_CORRECTION_COEFFICIENT              ((float)1.07872)

#define ADDITION_TIME_WHEN_SLEEP_FOR_1S             ((long) 53)
#define ADDITION_TIME_WHEN_BUTTON_IS_PRESSED_MS     ((long) -500)   // to compensate the time when we woke up via button press; sleep period is 1s, so 1/2 of it
// 57ms - 4 min ahead for 20 hours
// 56ms - ne znam, pak e +
// 54ms pak e +, okolo 45s za 10 4asa
// 53ms

// 52ms - izostava


#define Sprint(a)                                   Serial.print(a)
#define Sprintln(a)                                 Serial.println(a)

#endif // CONSTANTS_H
