#include "common.h"

#define PRESSURE_LOGGER_ARRAY_SIZE                  ((byte) 84) // 84
#define PERIOD_BETWEEN_LOG_ITEMS_MS                 ((long) 1000 * 60 * 2) //2 min

#define EEPROM_INDEX_LOG_ALTITUTE_DATA              ((int) 400)

/*
struct LogEntry
{
    int altituteValueInt;
};
*/

class PressureLogger
{
private:
    void pushNewItem(int value);
    char getNextPosition();

    // the valid numbers are from startPos until (endPos-1)
    long lastAddedTime;
    char lastIndex;

public:
    byte count;
    // LogEntry data[PRESSURE_LOGGER_ARRAY_SIZE];
    int data[PRESSURE_LOGGER_ARRAY_SIZE];

    PressureLogger()
    {
        lastAddedTime = 0;
        count = 0;
        lastIndex = -1;

//        count = 40;
//        for(int i = 0 ; i < 40; i++)
//        {
//            data[i].altituteValueInt = 10*(i + 1);
//        }
//        lastIndex = count - 1;
    }

    bool isTimeToLogEntry();

    char getFirstPosition();
   
//    char getPositionItemBack(char positionsBack);
  
 
    void print();

    void addEntry(int altituteValue);
};