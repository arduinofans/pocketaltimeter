#include <Arduino.h>
#include <EEPROM.h>
#include "eepromHelper.h"
#include "measuredValues.h"
#include "display.h" // if you remove it, you save flash space


GlobalSettings globalSettings;
extern MeasuredValues statistics;
// extern PressureLogger globalsPressureLogger;


// --------------------------------------------------------------------------------------
// EEPROM helper functions - BEGIN
void saveIntToEEPROM(int index, int value)
{
    byte * ptr = (byte*)&value;
    EEPROM.update(index, *ptr);
    EEPROM.update(index + 1, *(ptr + 1));
}

int readIntFromEEPROM(int index)
{
    int result;

    byte* ptr = (byte*) &result;
    *ptr = EEPROM.read(index);
    *(ptr+1) = EEPROM.read(index + 1);

    return result;
}
/*
void saveLongToEEPROM(int index, long value)
{
    byte * ptr = (byte*)&value;
    EEPROM.write(index, *ptr);
    EEPROM.write(index + 1, *(ptr + 1));
    EEPROM.write(index + 2, *(ptr + 2));
    EEPROM.write(index + 3, *(ptr + 3));
}

long readLongFromEEPROM(int index)
{
    long result;

    byte* ptr = (byte*) &result;
    *ptr = EEPROM.read(index);
    *(ptr+1) = EEPROM.read(index + 1);
    *(ptr+2) = EEPROM.read(index + 2);
    *(ptr+3) = EEPROM.read(index + 3);

    return result;
}
// EEPROM helper functions - END
// --------------------------------------------------------------------------------------
*/

// writes to EEPROM and updates the global value
void writeSetting(int eepromIndex, byte eepromValue)
{
    switch (eepromIndex)
    {
        case EEPROM_INDEX_CONTRAST:
            globalSettings.settingsValue_Contrast = eepromValue;
            break;
        case EEPROM_INDEX_TURN_OFF_DISPLAY_AFTER_MIN:
            globalSettings.settingsValue_TurnOffDisplayAfterMin = eepromValue;
            break;
        case EEPROM_INDEX_READ_SENSOR_PERIOD_S:
            globalSettings.settingsValue_ReadSensorPeriodS = eepromValue;
            break;
        case EEPROM_INDEX_CAN_WRITE_TO_EEPROM:
            globalSettings.canWriteToEEPROM = eepromValue;
            break;
        case EEPROM_INDEX_INTERPOLATE_ALTITUTE:
            globalSettings.settingsValue_InterpolateAltitute = eepromValue;
            break;
//        case EEPROM_INDEX_CALIBRATE_ALTITUTE_M:
//            globalSettings.settingsValue_CalibrateAltituteMeters = eepromValue;
//            break;
        case EEPROM_INDEX_BACKLIGHT:
            globalSettings.settingsValue_backlight = eepromValue;
            break;
    }
    
    EEPROM.update(eepromIndex, eepromValue);
}

void writeSetting2Bytes(int eepromIndex, int eepromValue)
{
    switch (eepromIndex)
    {
        case EEPROM_INDEX_CALIBRATE_ALTITUTE_M:
            globalSettings.settingsValue_CalibrateAltituteMeters = eepromValue;
            break;
    }
    
    saveIntToEEPROM(eepromIndex, eepromValue);
}


byte readSetting(int eepromIndex)
{
    return EEPROM.read(eepromIndex);
}

int readSetting2Bytes(int eepromIndex)
{
    return readIntFromEEPROM(eepromIndex);
}

void initEEPROMData()
{
    globalSettings.settingsValue_Contrast = readSetting(EEPROM_INDEX_CONTRAST);
    globalSettings.settingsValue_TurnOffDisplayAfterMin = readSetting(EEPROM_INDEX_TURN_OFF_DISPLAY_AFTER_MIN);
    globalSettings.settingsValue_ReadSensorPeriodS = readSetting(EEPROM_INDEX_READ_SENSOR_PERIOD_S);
    globalSettings.canWriteToEEPROM = (bool) readSetting(EEPROM_INDEX_CAN_WRITE_TO_EEPROM);
    globalSettings.settingsValue_InterpolateAltitute = (bool) readSetting(EEPROM_INDEX_INTERPOLATE_ALTITUTE);
    globalSettings.settingsValue_CalibrateAltituteMeters = (int) readSetting2Bytes(EEPROM_INDEX_CALIBRATE_ALTITUTE_M);

    globalSettings.settingsValue_backlight = (byte) readSetting(EEPROM_INDEX_BACKLIGHT);

    if(globalSettings.settingsValue_Contrast == 0) 
    {
        globalSettings.settingsValue_Contrast = DEFAULT_SETTING_VALUE_CONTRAST;
        writeSetting(EEPROM_INDEX_CONTRAST, globalSettings.settingsValue_Contrast);                
    }
    if(globalSettings.settingsValue_TurnOffDisplayAfterMin == 0) 
    {
        globalSettings.settingsValue_TurnOffDisplayAfterMin = DEFAULT_SETTING_VALUE_TURN_OFF_DISPLAY_AFTER_MIN;
        writeSetting(EEPROM_INDEX_TURN_OFF_DISPLAY_AFTER_MIN, globalSettings.settingsValue_TurnOffDisplayAfterMin);        
    }
    if(globalSettings.settingsValue_ReadSensorPeriodS == 0) 
    {
        globalSettings.settingsValue_ReadSensorPeriodS = DEFAULT_SETTING_VALUE_READ_SENSOR_PERIOD_S;
        writeSetting(EEPROM_INDEX_READ_SENSOR_PERIOD_S, globalSettings.settingsValue_ReadSensorPeriodS);    
    }
}

void resetEEPROMData()
{
    if(globalSettings.canWriteToEEPROM)
    {
        // write all config values to their init values
        globalSettings.settingsValue_Contrast = DEFAULT_SETTING_VALUE_CONTRAST;
        globalSettings.settingsValue_TurnOffDisplayAfterMin = DEFAULT_SETTING_VALUE_TURN_OFF_DISPLAY_AFTER_MIN;
        globalSettings.settingsValue_ReadSensorPeriodS = DEFAULT_SETTING_VALUE_READ_SENSOR_PERIOD_S;    
        globalSettings.canWriteToEEPROM = DEFAULT_SETTING_CAN_WRITE_TO_EEPROM;  
        globalSettings.settingsValue_InterpolateAltitute = DEFAULT_SETTING_INTERPOLATE_ALTITUTE;  
        globalSettings.settingsValue_CalibrateAltituteMeters = DEFAULT_SETTING_CALIBRATE_ALTITUTE_M;  
        globalSettings.settingsValue_backlight = DEFAULT_SETTING_BACKLIGHT;  

        writeSetting(EEPROM_INDEX_CONTRAST, globalSettings.settingsValue_Contrast);                
        writeSetting(EEPROM_INDEX_TURN_OFF_DISPLAY_AFTER_MIN, globalSettings.settingsValue_TurnOffDisplayAfterMin);        
        writeSetting(EEPROM_INDEX_READ_SENSOR_PERIOD_S, globalSettings.settingsValue_ReadSensorPeriodS);    
        writeSetting(EEPROM_INDEX_CAN_WRITE_TO_EEPROM, globalSettings.canWriteToEEPROM);    
        writeSetting(EEPROM_INDEX_INTERPOLATE_ALTITUTE, globalSettings.settingsValue_InterpolateAltitute);    
        writeSetting2Bytes(EEPROM_INDEX_CALIBRATE_ALTITUTE_M, globalSettings.settingsValue_CalibrateAltituteMeters);    
        writeSetting(EEPROM_INDEX_BACKLIGHT, globalSettings.settingsValue_backlight);    
    }
    else
    {
        showCenteredMessage(F("NO EEPROM"), 9);
    }
}

bool checkEEPROMIsInitialized()
{
    if(EEPROM.read(EEPROM_INDEX_PROGRAM_MARKER) != EEPROM_VALUE_PROGRAM_MARKER)
    {
        Serial.println(F("RESET !"));
        EEPROM.update(EEPROM_INDEX_PROGRAM_MARKER, EEPROM_VALUE_PROGRAM_MARKER);
        resetEEPROMData();
        statistics.resetData();
        return false;
    }
    return true;
}



void saveStatisticsToEEPROM()
{
    statistics.lastTimeSavedtoEEPROM = myMillis(); // this is outside, to avoid constant comming here
    if(globalSettings.canWriteToEEPROM)
    {
        statistics.eepromWriteCounter++;

        /*
        displayConfirm(F("save..."), 7, NULL, 0);
        int offset = EEPROM_INDEX_STATISTICS_DATA;
        saveIntToEEPROM(offset, prevSensorData.temperature);
    //    displayConfirm(F("."), 6, NULL, 0);
        offset += sizeof(prevSensorData.temperature);
        delay(5);
        saveIntToEEPROM(offset, prevSensorData.pressure);
        offset += sizeof(prevSensorData.pressure);
        delay(5);
        saveIntToEEPROM(offset, prevSensorData.humidity);
        offset += sizeof(prevSensorData.humidity);
        delay(5);

        saveIntToEEPROM(offset, eepromWriteCounter);
        offset += sizeof(eepromWriteCounter);
        saveIntToEEPROM(offset, temperature);
        offset += sizeof(temperature);
        saveIntToEEPROM(offset, humidity);
        offset += sizeof(humidity);
        saveIntToEEPROM(offset, pressure);
        offset += sizeof(pressure);
        saveLongToEEPROM(offset, altitute);
        offset += sizeof(altitute);
        EEPROM.write(offset, upDownFlag);
        offset += sizeof(upDownFlag);
        saveLongToEEPROM(offset, lastRegisteredAltitute);
        offset += sizeof(lastRegisteredAltitute);
        delay(5);
        saveIntToEEPROM(offset, tempDenivelationUp);
        offset += sizeof(tempDenivelationUp);
        saveIntToEEPROM(offset, tempDenivelationDown);
        offset += sizeof(tempDenivelationDown);
        saveIntToEEPROM(offset, maxTempDenivelationUp);
        offset += sizeof(maxTempDenivelationUp);
        saveIntToEEPROM(offset, maxTempDenivelationDown);
        offset += sizeof(maxTempDenivelationDown);
        delay(5);
        saveLongToEEPROM(offset, totalDenivelationUp);
        offset += sizeof(totalDenivelationUp);
        saveLongToEEPROM(offset, totalDenivelationDown);
        offset += sizeof(totalDenivelationDown);
        delay(5);
        saveIntToEEPROM(offset, minTemperature);
        offset += sizeof(minTemperature);
        saveIntToEEPROM(offset, maxTemperature);
        offset += sizeof(maxTemperature);
        saveIntToEEPROM(offset, minHumidity);
        offset += sizeof(minHumidity);
        saveIntToEEPROM(offset, maxHumidity);
        offset += sizeof(maxHumidity);
        saveIntToEEPROM(offset, minPressure);
        offset += sizeof(minPressure);
        saveIntToEEPROM(offset, maxPressure);
        offset += sizeof(maxPressure);
        saveIntToEEPROM(offset, minAltitute);
        offset += sizeof(minAltitute);
        delay(5);

        saveLongToEEPROM(offset, maxAltitute);
        offset += sizeof(maxAltitute);
        delay(5);
        displayConfirm(F("...save"), 6, NULL, 0);

       // byte isChangedFlags[3]; // store flags for all monitoring values if they are changed

        */

        byte size = sizeof(statistics);
        byte* obj = (byte*) (void*) &statistics;
    //        Sprint("size = ");
    //        Sprintln(size);
        for(int i = 0; i < size; i++)
        {
            EEPROM.update(i + EEPROM_INDEX_STATISTICS_DATA, *(obj + i));
    //            Sprint(i);Sprint(" ");
    //          Sprintln(obj[i]);
        }
    }    
    else
    {
        showCenteredMessage(F("NO EEPROM"), 9);
    }
};
void readStatisticsFromEEPROM()
{
    /*
    displayConfirm(F("read..."), 7, NULL, 0);

    int offset = EEPROM_INDEX_STATISTICS_DATA;

    prevSensorData.temperature = readIntFromEEPROM(offset);
    offset += sizeof(prevSensorData.temperature);

    prevSensorData.pressure = readIntFromEEPROM(offset);
    offset += sizeof(prevSensorData.pressure);

    prevSensorData.humidity = readIntFromEEPROM(offset);
    offset += sizeof(prevSensorData.humidity);

    eepromWriteCounter = readIntFromEEPROM(offset);
    offset += sizeof(eepromWriteCounter);

    temperature = readIntFromEEPROM(offset);
    offset += sizeof(temperature);

    humidity = readIntFromEEPROM(offset);
    offset += sizeof(humidity);

    pressure = readIntFromEEPROM(offset);
    offset += sizeof(pressure);

    altitute = readLongFromEEPROM(offset);
    offset += sizeof(altitute);

    upDownFlag = EEPROM.read(offset);
    offset += sizeof(upDownFlag);

    lastRegisteredAltitute = readLongFromEEPROM(offset);
    offset += sizeof(lastRegisteredAltitute);

    tempDenivelationUp = readIntFromEEPROM(offset);
    offset += sizeof(tempDenivelationUp);
    tempDenivelationDown = readIntFromEEPROM(offset);
    offset += sizeof(tempDenivelationDown);
    maxTempDenivelationUp = readIntFromEEPROM(offset);
    offset += sizeof(maxTempDenivelationUp);
    maxTempDenivelationDown = readIntFromEEPROM(offset);
    offset += sizeof(maxTempDenivelationDown);

    totalDenivelationUp = readLongFromEEPROM(offset);
    offset += sizeof(totalDenivelationUp);
    totalDenivelationDown = readLongFromEEPROM(offset);
    offset += sizeof(totalDenivelationDown);

    minTemperature = readIntFromEEPROM(offset);
    offset += sizeof(minTemperature);
    maxTemperature = readIntFromEEPROM(offset);
    offset += sizeof(maxTemperature);
    minHumidity = readIntFromEEPROM(offset);
    offset += sizeof(minHumidity);
    maxHumidity = readIntFromEEPROM(offset);
    offset += sizeof(maxHumidity);
    minPressure = readIntFromEEPROM(offset);
    offset += sizeof(minPressure);
    maxPressure = readIntFromEEPROM(offset);
    offset += sizeof(maxPressure);
    minAltitute = readIntFromEEPROM(offset);
    offset += sizeof(minAltitute);

    maxAltitute = readLongFromEEPROM(offset);
    offset += sizeof(maxAltitute);
 
    displayConfirm(F("...read"), 7, NULL, 0);
    */
    
    int size = sizeof(statistics);
//        Sprint("size = ");
//        Sprintln(size);
    byte* obj = (byte*)(void*) &statistics;
    for(int i = 0; i < size; i++)
    {
//            Sprint(i);Sprint(" ");
//            Sprintln(obj[i]);
        *(obj + i) = EEPROM.read(i + EEPROM_INDEX_STATISTICS_DATA);
    }
    

    // those two shout be reset to 0
    statistics.lastTimeTemperatureIsMeasuredMs = 0;
    statistics.lastTimeSavedtoEEPROM = 0;
};

