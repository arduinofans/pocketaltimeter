#include "constants.h"
#include "common.h"
#include "buttons.h"

GlobalButtons globalButtons;


GlobalButtons::GlobalButtons()
{
    working_time_ms = myMillis();

    pinInterruptFlag = InterruptStates_NormalWork; // no interrupt, normal

    button1State = ButtonState_NoPressed;
    button3State = ButtonState_NoPressed;
    button4State = ButtonState_NoPressed;
    isHandledButtonPress = true;
}

void GlobalButtons::renew_working_timer()
{
    working_time_ms = myMillis();
}
void GlobalButtons::renew_working_timer_for_deep_sleep()
{
    working_time_ms_for_deep_sleep = myMillis();
}

ButtonStates GlobalButtons::ReadButtonsState()
{
    bool button1NowPressed = digitalRead(PIN_button1) == LOW;
    bool button3NowPressed = digitalRead(PIN_button3) == LOW;
    bool button4NowPressed = digitalRead(PIN_button4) == LOW;
    delay(30);  // important to have a delay !

    ReadSingleButtonState(&button1State, button1NowPressed, &timeWhenButton1WasPressed);
    ReadSingleButtonState(&button3State, button3NowPressed, &timeWhenButton3WasPressed);
    ReadSingleButtonState(&button4State, button4NowPressed, &timeWhenButton4WasPressed);

    if(button1State == ButtonState_NoPressed && button3State == ButtonState_NoPressed && button4State == ButtonState_NoPressed)
    {
        allButtonStates = AllButtonState_NoPressed; // clear states
        isHandledButtonPress = true;                // clear the flag
    }
    else if(button1State == ButtonState_Pressed && button3State == ButtonState_Pressed && button4State == ButtonState_Pressed)
    {
        allButtonStates = AllButtonState_Btn1_Btn3_Btn4_Pressed;
    }
    else if(button1State == ButtonState_Pressed && button4State == ButtonState_Pressed)
    {
        allButtonStates = AllButtonState_Btn1_Btn4_Pressed;
    }
    else if(button1State == ButtonState_Pressed && button3State == ButtonState_Pressed)
    {
        allButtonStates = AllButtonState_Btn1_Btn3_Pressed;
    }
    else if(button3State == ButtonState_Pressed && button4State == ButtonState_Pressed)
    {
        allButtonStates = AllButtonState_Btn3_Btn4_Pressed;
    } 
    else if(button1State == ButtonState_ShortReleased)
    {
        allButtonStates = AllButtonState_Btn1_ShortReleased;
    }
    else if(button1State == ButtonState_LongPressed)
    {
        allButtonStates = AllButtonState_Btn1_LongPressed;
    }
    else if(button3State == ButtonState_ShortReleased)
    {
        allButtonStates = AllButtonState_Btn3_ShortReleased;
    }
    else if(button3State == ButtonState_LongPressed)
    {
        allButtonStates = AllButtonState_Btn3_LongPressed;
    }
    else if(button4State == ButtonState_ShortReleased)
    {
        allButtonStates = AllButtonState_Btn4_ShortReleased;
    }
    else if(button4State == ButtonState_LongPressed)
    {
        allButtonStates = AllButtonState_Btn4_LongPressed;
    }
    else if(button1State == ButtonState_Pressed || button3State == ButtonState_Pressed || button4State == ButtonState_Pressed)
    {
        // clear states - waiting for a new state
        allButtonStates = AllButtonState_NoPressed;
    }

    // reset sleep counter when buttons are pressed
    if(button1NowPressed || button3NowPressed || button4NowPressed )
    {
        renew_working_timer();
        renew_working_timer_for_deep_sleep();
    }
}

// this function is responsible to handle the button press/state of a single button, later all are combined
void GlobalButtons::ReadSingleButtonState(ButtonStates* buttonState, bool buttonIsNowPressed, long* timeWhenButtonWasPressed)
{
    if(*buttonState == ButtonState_NoPressed && buttonIsNowPressed == false)
    {
        // do nothing, just ignore it
    }
    else if(*buttonState == ButtonState_NoPressed && buttonIsNowPressed)
    {
        // is pressed for first time
        *timeWhenButtonWasPressed = myMillis();
        *buttonState = ButtonState_Pressed;
        isHandledButtonPress = false;
    }
    else if(*timeWhenButtonWasPressed > 0 && (myMillis() - *timeWhenButtonWasPressed > LOND_PRESS_TIME) && buttonIsNowPressed == true)
    {
        // long click
        *buttonState = ButtonState_LongPressed;
    }        
    else if(*buttonState == ButtonState_Pressed && buttonIsNowPressed == false)
    {
        // short released
        *timeWhenButtonWasPressed = 0;
        *buttonState = ButtonState_ShortReleased;
    }
    else if(buttonIsNowPressed == false)
    {
        // clear remaining state, for example rease of button after short press or long press
        *buttonState = ButtonState_NoPressed;
        *timeWhenButtonWasPressed = 0;
    }
}


// this function is used just for test purposes to know which button is pressed
void MonitorButtons()
{
    if(globalButtons.isHandledButtonPress == false)
    {
        if(globalButtons.allButtonStates == AllButtonState_Btn1_Btn3_Btn4_Pressed)
        {
            Sprintln();
            Sprintln(F("$$$ - ALL BUTTONS are pressed!"));
        }
        if(globalButtons.allButtonStates == AllButtonState_Btn1_Btn4_Pressed)
        {
            Sprintln();
            Sprintln(F("$$$ 1-4 buttons ..."));
        }
        if(globalButtons.allButtonStates == AllButtonState_Btn1_Btn3_Pressed)
        {
            Sprintln();
            Sprintln(F("$$$ 1 - 3 buttons..."));
        }
        if(globalButtons.allButtonStates == AllButtonState_Btn1_ShortReleased)
        {
            Sprintln();
            Sprintln(F("$$$ short pressed 1 ..."));
        }
        if(globalButtons.allButtonStates == AllButtonState_Btn1_LongPressed  )
        {
            Sprintln();
            Sprintln(F("$$$ long pressed 1 ..."));
        }

        if(globalButtons.allButtonStates == AllButtonState_Btn3_ShortReleased)
        {
            Sprintln();
            Sprintln(F("$$$ short pressed 3 ..."));
        }
        if(globalButtons.allButtonStates == AllButtonState_Btn3_LongPressed  )
        {
            Sprintln();
            Sprintln(F("$$$ long pressed 3 ..."));
        }

        if(globalButtons.allButtonStates == AllButtonState_Btn4_ShortReleased)
        {
            Sprintln();
            Sprintln(F("$$$ short pressed 4 ..."));
        }
        if(globalButtons.allButtonStates == AllButtonState_Btn4_LongPressed )
        {
            Sprintln();
            Sprintln(F("$$$ long pressed 4 ..."));
        }
    }
}
