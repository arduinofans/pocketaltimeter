#include "constants.h"
#include "pressureLogger.h"

PressureLogger globalsPressureLogger;

void PressureLogger::pushNewItem(int value)
{
    char nextIndex = getNextPosition();
    data[nextIndex] = value;
    lastIndex = nextIndex;
    if(count < PRESSURE_LOGGER_ARRAY_SIZE)
    {
        count++;
    }
}

char PressureLogger::getNextPosition()
{
    return (lastIndex + 1) % PRESSURE_LOGGER_ARRAY_SIZE;
}


bool PressureLogger::isTimeToLogEntry()
{
    return  myMillis() - lastAddedTime > PERIOD_BETWEEN_LOG_ITEMS_MS;
}

char PressureLogger::getFirstPosition()
{
    char result = lastIndex - count + 1;
    if(result < 0)
    {
        result = PRESSURE_LOGGER_ARRAY_SIZE + result;
    }
    return result;
}

// todo - remove this function later
void PressureLogger::print()
{
    char startPos = getFirstPosition();
    Sprint("startPos =  ");
    Sprintln(startPos);
    for(int i = 0; i < count; i++)
    {
        Sprint(" ");
        Sprint(data[(startPos + i) % PRESSURE_LOGGER_ARRAY_SIZE]);
    }
    Sprintln("");
}
/*
char PressureLogger::getPositionItemBack(char positionsBack)
{
    char result = lastIndex - positionsBack;
    if(result < 0)
    {
        result = PRESSURE_LOGGER_ARRAY_SIZE + result;
    }
    return result;
}
*/

void PressureLogger::addEntry(int altituteValue)
{
    Sprint(F("adding new altitute: "));
    Sprintln(altituteValue);

 //   Sprint("count: ");
 //   Sprintln(count);
//    Sprint("lastIndex: ");
//    Sprintln(lastIndex);

    if(count == 0)
    {
        count = 1;
        data[0] = altituteValue;
        lastIndex = 0;
    }
    else
    {
        // add empty positions if the measurement is much later than previous measure
        byte emptyItems = (myMillis() - lastAddedTime) / (PERIOD_BETWEEN_LOG_ITEMS_MS);
        emptyItems = emptyItems - 1; // do not count the new item
        for(int i = 0; i < emptyItems && i < PRESSURE_LOGGER_ARRAY_SIZE; i++)
        {
            pushNewItem(0);
//            char nextIndex = getNextPosition();
//            data[nextIndex].altituteValueInt = 0;
//            lastIndex = nextIndex;
//            if(count < ARRAY_SIZE)
//            {
//                count++;
//            }
        }

        // add the actual value to the array
        pushNewItem(altituteValue);
//        char nextIndex = getNextPosition();
//        data[nextIndex].altituteValueInt = altituteValue;
//        lastIndex = nextIndex;
//        if(count < ARRAY_SIZE)
//        {
//            count++;
//        }
    }

    
      //  Sprintln("--- after -- ");
 //       Sprint("count: ");
  //      Sprintln(count);
     //   Sprint("lastIndex: ");
     //   Sprintln(lastIndex);

      //  print();

        lastAddedTime = myMillis();
}
