#ifndef CUSTOM_SLEEP_H
#define CUSTOM_SLEEP_H

extern GlobalButtons globalButtons;
extern MeasuredValues statistics;

#include "display.h"
#include "StateMachine.h" // used only to refresh the screen when read data

extern GlobalStates globalStates;

void pin2_isr()
{
    if(globalButtons.pinInterruptFlag != InterruptStates_Interrupted)
    {
        increaseDiscrepancy(ADDITION_TIME_WHEN_BUTTON_IS_PRESSED_MS);
    }

    globalButtons.pinInterruptFlag = InterruptStates_Interrupted; // interrupted!
}



void sleepNow() {  
    // turn off LED not to drain power
//    digitalWrite(PIN_led_big, LOW);

    sleep_enable();                         // enables the sleep bit in the mcucr register  
    attachInterrupt(0,pin2_isr, LOW);       // use interrupt 0 (pin 2) and run function  
    attachInterrupt(1,pin2_isr, LOW);       // use interrupt 1 (pin 3) and run function  
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);    // sleep mode is set here  
    cli();
    sleep_bod_disable();
    sei();
    sleep_cpu();
    sleep_mode();                           // here the device is actually put to sleep!!  

    // THE PROGRAM CONTINUES FROM HERE AFTER WAKING UP  
    sleep_disable();         // first thing after waking from sleep: disable sleep...  
    detachInterrupt(0);      // disables interrupt 0 on pin 2 so the wakeUpNow code will not be executed during normal running time.  
    detachInterrupt(1);

    globalButtons.renew_working_timer_for_deep_sleep();

    // may be it is not needed to read the buttons state here, if sleeping is at the end of loop()
    //globalButtons.ReadButtonsState();
}  

// we use this methos as forever sleep
void gotoDeepSleep(unsigned long sleepTimeS)
{
    Sprint(F("deep sleep for s.."));
    Sprintln(sleepTimeS);
    delay(10);
    globalButtons.pinInterruptFlag = InterruptStates_DeepSleep;

    byte sleepFrameSize = 1;// 4 = SLEEP_4S or 8
    unsigned long sleepCounter = 1 + ((sleepTimeS - 1) / sleepFrameSize);
//    Sprintln(sleepCounter);
//    delay(30);

    displayHeaderSleepState(true);

    // is it OK when it is here, but not in the loop?
    attachInterrupt(0, pin2_isr, LOW);
    attachInterrupt(1, pin2_isr, LOW);

    for(unsigned long i = 0; (i < sleepCounter) && (globalButtons.pinInterruptFlag == InterruptStates_DeepSleep); i++)
    {
    //    Sprintln(String("pillow..."));
   //     delay(30);

        /*wdt_enable((long)sleepTimeS * 1000);
        WDTCSR |= (1 << WDIE);    
        lowPowerBodOff(SLEEP_MODE_PWR_DOWN);
        */
        freezeMillis(sleepFrameSize*1000);
        LowPower.powerDown(SLEEP_1S, ADC_OFF, BOD_OFF); 
        defrostMillis();

        // read sensor data if it is time to do it; and update the screen if it is active
        // this also turns off the display if it is time in PerformAction()
        if(globalButtons.pinInterruptFlag == InterruptStates_DeepSleep && statistics.isTimeToReadSensorData())
        {
            ReadSensorData();
            if(globalStates.isDisplayActive)
            {
                globalStates.currentStateObject->PerformAction();

                // TODO - should it be in a common function for the whole header?
                // update the header
                displayHeaderVoltage();
                displayTimer();
            }
        }

  //      Sprintln("breath...");
    //    delay(20);
    }

    // !!! has to be immediately after the wake up !!!
    sleep_disable();
    detachInterrupt(0);
    detachInterrupt(1);

    // natural wake up - reset working timer
    //if(globalButtons.pinInterruptFlag == InterruptStates_DeepSleep)
    //{
        // new fix pin2_isr();
        globalButtons.renew_working_timer_for_deep_sleep();
    //}

    // turn on the display if it is switched off and a forced wake up is performed
    if(globalButtons.pinInterruptFlag == InterruptStates_Interrupted && globalStates.isDisplayActive == false)
    {
        globalButtons.renew_working_timer();
        displayON();
        //globalStates.turnOnDisplay();
    }

    // re-init the interrupt flag!
    globalButtons.pinInterruptFlag = InterruptStates_NormalWork;

    displayHeaderSleepState(false);

    // clear the button presses
    // todo - is it OK, or add a flag to clear it? - we can not handle long click
    //while(digitalRead(PIN_button1) == LOW || digitalRead(PIN_button4) == LOW);
 //   delay(300);

    Sprintln(F("waking up..."));
    delay(10);

}

#endif // CUSTOM_SLEEP_H
