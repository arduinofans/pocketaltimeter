#include <Arduino.h>

//#include "timer.h"
#include "common.h"
#include "constants.h"

long millisDiscrepancy = 0;
long freezeTime = 0;
long freezePeriod = 0;

long myMillis()
{
    if(freezePeriod > 0)
    {
        Sprintln(F("!!! SEVERE ERROR !!! You called myMillis() before defrosting millis !"));
        delay(10000);
    }

    // in case we overrun the long size, clear the discreapncy
    long result = millis() + millisDiscrepancy;
    if (result < 0)
    {
        millisDiscrepancy = 0;
    }
    return result;
}


void freezeMillis(long periodMs)
{
    if(freezePeriod > 0)
    {
        Sprintln(F("!!! SEVERE ERROR !!! You called freezeMillis() before defrosting millis !"));
        delay(10000);
    }
    freezeTime = millis();
    freezePeriod = periodMs;

    millisDiscrepancy += periodMs / 1000 * ADDITION_TIME_WHEN_SLEEP_FOR_1S;
}

void defrostMillis()
{
    long discr = freezeTime - millis() + freezePeriod;
    millisDiscrepancy += discr;
    freezePeriod = 0;
}

void increaseDiscrepancy(long periodMs)
{
    millisDiscrepancy += periodMs;
}


// move those two functions to common place
float getFloatValueFromInt(int value)
{
    return (float)value / 10;
}
float getFloatValueFromLong(long value)
{
    return (float)value / 10;
}


/*
long readVcc() {
  long result;
  // Read 1.1V reference against AVcc

  Sprintln(1);

    ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
    Sprintln(2);

  delay(2); // Wait for Vref to settle
  noInterrupts ();
    Sprintln(3);

  // start the conversion
  ADCSRA |= _BV (ADSC) | _BV (ADIE);
    Sprintln(4);

 // set_sleep_mode (SLEEP_MODE_ADC);    // sleep during sample
    Sprintln(5);

  interrupts ();
      Sprintln(6);

  sleep_mode (); 
    Sprintln(7);

  // reading should be done, but better make sure
  // maybe the timer interrupt fired 
  while (bit_is_set(ADCSRA,ADSC));
  result = ADCL;
  result |= ADCH<<8;
  result = 1126400L / result; // Back-calculate AVcc in mV

  return result;
}


// Battery measure
int getBatteryLevel () 
{
  int results = (readVcc() - 2000)  / 10;   

  if (results > 100)
    results = 100;
  if (results < 0)
    results = 0;
  return results;
} // end of getBandgap
*/

// when ADC completed, take an interrupt 
//EMPTY_INTERRUPT (ADC_vect);


