#ifndef STATE_MACHINE_H
#define STATE_MACHINE_H

#include "Arduino.h"

#include "buttons.h"
#include "eepromHelper.h"
#include "measuredValues.h"
#include "display.h"
//#include "timer.h"


// enum with ALL possible states in the application
enum states
{
    state_normal,                
    state_tracking,
    state_summary,
    state_lap_summary,
    state_setttings,
    state_message_box
};

// enum with all sub-screens in the settings screen
enum settings_screens
{
    settings_screen_set_contrast,
    settings_screen_turn_off_display_after_min,
    settings_screen_read_sensor_every_s,
    settings_screen_reset_all,
    settings_screen_interpolate_altitute,
    settings_screen_calibrate_altitute,
    settings_screen_backlight,
};

// enum with all sub-screens in the summary screen
enum summary_screens
{
    summary_screen_1,      
    summary_screen_2,      
    summary_screen_3,      
    summary_screen_4
};


// ------------------------------------------------------------------------------------------------
// Base class that each state will inherit
// ------------------------------------------------------------------------------------------------
class BaseState
{
public:
    int sleepTimeInMs;
//    int deepSleepTimeInS;

protected:
    int activationTime;

public:
    virtual void ActivateState();
    virtual void PerformAction() = 0;
};


// ------------------------------------------------------------------------------------------------
// The normal state class - display all sensor information
// ------------------------------------------------------------------------------------------------
class NormalState : public BaseState
{
public:
    NormalState();
    void ActivateState();
    void PerformAction() override;
};

// ------------------------------------------------------------------------------------------------
// The tracking state class - display only some tracking information
// ------------------------------------------------------------------------------------------------
class TrackingState : public BaseState
{
public:
    TrackingState();
    void PerformAction() override;
    void ActivateState();
};

// ------------------------------------------------------------------------------------------------
// The Settings state class - all screens which set settings to the system are here
// ------------------------------------------------------------------------------------------------
class SettingsState : public BaseState
{
private:
    settings_screens current_screen;
    byte currentScreenSettingValue;
    int currentScreenSettingValue2Bytes;
    unsigned long resetActivationTime;
    bool flagCalibrationDirection; // if we have to increment ot decrement direction when calibrating altitute


public:
    SettingsState();
    void PerformAction() override;

private:
    void ActivateState();
    void gotoNextSetting();
    void readCurrentSettingValue(); // set value to currentScreenSettingValue, taken from the global variables
    void writeCurrentSettingValue(byte newValue);
    void writeCurrentSettingValue2Bytes(int newValue);
    void findNextSettingsValue(int param = 0); // param - optional parameter; so far is used to set calibration denivalation steps
};

// ------------------------------------------------------------------------------------------------
// The Summary state class - all screens which show summary info are here (so far 2)
// ------------------------------------------------------------------------------------------------
class SummaryState : public BaseState
{
private:
    summary_screens current_screen;

public:
    SummaryState();
    void PerformAction() override;

private:
    void ActivateState();
    void gotoNextSummary();
};


// ------------------------------------------------------------------------------------------------
// The Lap Summary state class - display summary for the laps
// ------------------------------------------------------------------------------------------------
class LapSummaryState : public BaseState
{
private:
//    summary_screens current_screen;
    byte currentLapToView;
public:
    LapSummaryState();
    void PerformAction() override;

private:
    void ActivateState();
    void gotoNextSummary();
};

// ------------------------------------------------------------------------------------------------
// The MessageBox state class - when you want to show a modal message at the center of the screen
// ------------------------------------------------------------------------------------------------
class MessageBoxState : public BaseState
{
private:
    __FlashStringHelper*  message;

public:
    MessageBoxState();
    void PerformAction() override;
    void setMessage(const __FlashStringHelper* newMessage);

private:
    void ActivateState() override;
};


// ------------------------------------------------------------------------------------------------
// The most IMPORTANT class - this is the class which dispatches which is the current state
// ------------------------------------------------------------------------------------------------
class GlobalStates
{
public:
    bool isDisplayActive;
    bool forceDisplayRedraw;

    long lastTimeBatteryCheckMs;        // TODO - where this has to be placed?

    states currentState;
    BaseState* currentStateObject;
    states previousState;

    byte lastSec; // keep track if the last millis() - it is displayed so far on the screen only
    byte lastMin;
    byte lastHour;
    byte lastDay;

private:
    NormalState normalState;
    TrackingState trackingState;
    SettingsState settingsState;
    SummaryState summaryState;
    LapSummaryState lapSummaryState;
public:
    MessageBoxState messageBoxState; // it is public to be set from the main loop()

public:
    GlobalStates();
    void init();

    void setNewState(states newState);
    void ToggleDisplay();
    void turnOffDisplay();
    void turnOnDisplay();
};

#endif // STATE_MACHINE_H
