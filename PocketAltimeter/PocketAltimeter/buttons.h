#ifndef BUTTONS_H
#define BUTTONS_H

#include "Arduino.h"
#include <avr/sleep.h>
#include "libraries\lowPower\LowPower.h"

// an enum with all button states that are available for the system; it is accessible from other files also
enum AllButtonStates{
    AllButtonState_NoPressed = 0,
    AllButtonState_Btn1_ShortReleased = 1,
    AllButtonState_Btn1_LongPressed = 2,
    AllButtonState_Btn4_ShortReleased = 3,
    AllButtonState_Btn4_LongPressed = 4,
    AllButtonState_Btn3_ShortReleased = 5,
    AllButtonState_Btn3_LongPressed = 6,
    AllButtonState_Btn1_Btn3_Pressed = 7,
    AllButtonState_Btn1_Btn4_Pressed = 8,
    AllButtonState_Btn3_Btn4_Pressed = 9,
    AllButtonState_Btn1_Btn3_Btn4_Pressed = 12
};

// enum with the possible states of a single button; it is used only in this file
enum ButtonStates{
    ButtonState_NoPressed = 0,
    ButtonState_Pressed = 1,
    ButtonState_ShortReleased = 2,
    ButtonState_LongPressed = 3
};

enum InterruptStates
{
    InterruptStates_NormalWork,
    InterruptStates_Interrupted,
    InterruptStates_DeepSleep
};

class GlobalButtons
{
public:
    long working_time_ms;                   // renew the timer after button is pressed; a 'timer' to monitor how long the system stays idle after the last button is pressed
    // TODO: - do we need this timer any more? now we do not have wake up after sleep!
    long working_time_ms_for_deep_sleep;    // renew the timer when a button is pressed or a system wakes up
    InterruptStates pinInterruptFlag;       // a flag if there is a pin 2,3 interrupt; it can also be used to know when we are sleeping ( == 0)

    // button properties
    bool isHandledButtonPress;
    AllButtonStates allButtonStates;

private:
    ButtonStates button1State;
    ButtonStates button3State;
    ButtonStates button4State;
    long timeWhenButton1WasPressed;
    long timeWhenButton3WasPressed;
    long timeWhenButton4WasPressed;

public:
    GlobalButtons();
    void renew_working_timer();
    void renew_working_timer_for_deep_sleep(); // TODO: - do we need this timer any more?

    ButtonStates ReadButtonsState();

    // this function is responsible to handle the button press/state of a single button, later all are combined
    void ReadSingleButtonState(ButtonStates* buttonState, bool buttonIsNowPressed, long* timeWhenButtonWasPressed);
};

// this function is used just for test purposes to know which button is pressed
void MonitorButtons();

#endif // BUTTONS_H

