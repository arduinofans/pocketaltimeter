#include "libraries\bme280_Drotek\BME280.h"
#include "measuredValues.h"
#include "common.h"
#include "display.h"
#include "pressureLogger.h"

MeasuredValues statistics;
extern PressureLogger globalsPressureLogger;

//static int xx = 0;

    
bool MeasuredValues::isTimeToReadSensorData()
{
    bool hasToRead = myMillis() - lastTimeTemperatureIsMeasuredMs 
        > (long) 1000 * SETTINGS_ARRAY_READ_SENSOR_PERIOD_MULTIPLYER * globalSettings.settingsValue_ReadSensorPeriodS;
    return hasToRead;
}

bool MeasuredValues::isTimeToSaveSensorData()
{
    bool hasToSave = myMillis() - lastTimeSavedtoEEPROM > (SAVE_STATISTICS_2_EEPROM_MIN * 60 * 1000)          // every 3 mins and has some denivelation done
            && (totalDenivelationDown + totalDenivelationUp - sumTotalDenivelationWhenSaving > DENIVELATION_NEEDED_TO_SAVE_TO_EEPROM_DM);
    hasToSave = hasToSave || (lastTimeSavedtoEEPROM == 0 && millis() > 20000); // save the first time after 20s

	//if(hasToSave)
	//{
	//	Sprint("millis() = ");
	//	Sprintln(millis());
	//	Sprint("lastTimeSavedtoEEPROM = ");
	//	Sprintln(lastTimeSavedtoEEPROM);
	//	Sprint("hasToSave = ");
	//	Sprintln(hasToSave);
	//}
    return hasToSave;
}

bool MeasuredValues::evaluateMeasurements(int newTemperatureInt, int newPressureInt, int newHumidityInt)
{
    bool result = true;
    if(prevSensorData.pressure == INITIAL_DATA)
    {
        result = false;
    }
    else 
    {
       // int difference = newTemperatureInt - prevSensorData.temperature ;
      //  bool tempIsSuspisious = difference > 20 || difference < -20;        // 2C

        int difference = newPressureInt - prevSensorData.pressure ;
        bool pressureIsSuspisious = difference > 20 || difference < -20;    // 2hPa

     //   difference = newHumidityInt - prevSensorData.humidity;
    //    bool humidityIsSuspisious = difference > 30 || difference < -30;    // 3%

        // TODO: this is a temp for calibration
    /*    if(tempIsSuspisious)
        {
            displayConfirm(F("Wrong T."), 8, NULL, 0);
        }
        if(humidityIsSuspisious)
        {
            displayConfirm(F("Wrong Hum"), 9, NULL, 0);
        }*/
        if(pressureIsSuspisious)
        {
            displayConfirm(F("Wrong Pr."), 9, NULL, 0);
        }

        if(/*tempIsSuspisious || humidityIsSuspisious ||*/ pressureIsSuspisious)
        {
            result = false;
            // todo - display message?
            Sprintln(F("Wrong sensor data detected"));
        }
    }

    return result;
}

void MeasuredValues::interpolateAltitute(int& newPressure, long& newAltitute)
{
    
//        Serial.print("prevSensorData.altitute = ");
//        Serial.println(prevSensorData.altitute);
//        Serial.print("newAltitute = ");
//        Serial.println(newAltitute);
    if(prevSensorData.pressure != INITIAL_DATA)
    {
        newPressure = ((newPressure  * 2) + prevSensorData.pressure ) / 3;
        newAltitute = ((newAltitute  * 2) + prevSensorData.altitute ) / 3;

//        Serial.print("altitute = ");
//        Serial.println(newAltitute);
    }
}

void MeasuredValues::setNewValues(float& newTemperature, float& newPressure, float& newAltitute, float& newHumidity)
{
    int newTemperatureInt = (int)(newTemperature * 10);
    int newPressureInt = (int)(newPressure * 10);
    long newAltituteInt = (long)(newAltitute * 10);
    int newHumidityInt = (int)(newHumidity * 10);
    altituteRaw = newAltituteInt;

    // calibrate the altitute and save ti
    newAltituteInt += globalSettings.settingsValue_CalibrateAltituteMeters * 10;
    newAltitute = (float) newAltituteInt / 10;

    bool isFirstMeasure = minTemperature == INITIAL_DATA;

    if(globalSettings.settingsValue_InterpolateAltitute && isFirstMeasure == false)
    {
        interpolateAltitute(newPressureInt, newAltituteInt);
    }

    bool measreumentsAreValid = evaluateMeasurements(newTemperatureInt, newPressureInt, newHumidityInt);
    if(measreumentsAreValid)
    {
        if(newTemperatureInt != temperature)
        {
            setFlagBit(FLAG_INDEX_TEMPERATURE, true);
            temperature = newTemperatureInt;
            if(newTemperatureInt < minTemperature) minTemperature = newTemperatureInt;
            if(newTemperatureInt > maxTemperature) maxTemperature = newTemperatureInt;
        }
        if(newHumidityInt != humidity)
        {
            setFlagBit(FLAG_INDEX_HUMIDITY, true);
            humidity = newHumidityInt;
            if(newHumidityInt < minHumidity) minHumidity = newHumidityInt;
            if(newHumidityInt > maxHumidity) maxHumidity = newHumidityInt;
        }
        if(newPressureInt != pressure)
        {
            setFlagBit(FLAG_INDEX_PRESSURE, true);
            pressure = newPressureInt;
            if(newPressureInt < minPressure) minPressure = newPressureInt;
            if(newPressureInt > maxPressure) maxPressure = newPressureInt;
        }
        if(newAltituteInt != altitute)
        {
            setFlagBit(FLAG_INDEX_ALTITUTE, true);

            altitute = newAltituteInt;

            // add a pointer to the laps, to save space in the flash
            LapStatistics* currentLap = NULL;
            if(statistics.currentLapIndex >= 0)
            {
                currentLap = &statistics.lapStatistics[statistics.currentLapIndex];
            }

            if(isFirstMeasure == false)
            {
                // change the tempDenivaltion values and totalDenivation values
                if(upDownFlag == true)
                {
                    if(newAltituteInt - lastRegisteredAltitute >= DENIVELATION_NEEDED_TO_MARK_DM)
                    {
                        long moveUpDeniv = newAltituteInt - lastRegisteredAltitute;
                        totalDenivelationUp += moveUpDeniv;
                        if(statistics.currentLapIndex >= 0)
                        {
                            currentLap->lapTotalDenivelationUp += moveUpDeniv;
                            currentLap->lapMaxAltitute = max(currentLap->lapMaxAltitute, altitute);
                        }
                        tempDenivelationUp += moveUpDeniv;
                        maxTempDenivelationUp = max(maxTempDenivelationUp, tempDenivelationUp);
                        lastRegisteredAltitute = newAltituteInt;
                        setFlagBit(FLAG_INDEX_LAST_REGISTERED_ALTITUTE, true);
                        setFlagBit(FLAG_INDEX_TEMP_DENIVELATION_UP, true);
                        setFlagBit(FLAG_INDEX_TOTAL_DENIVELATION_UP, true);
                        setFlagBit(FLAG_INDEX_LAP_DENIVELATION_UP, true);
                    }
                    else if(lastRegisteredAltitute - newAltituteInt >= DENIVELATION_NEEDED_TO_MARK_DM)
                    {
                        tempDenivelationUp = 0;
                        tempDenivelationDown = 0;
                        upDownFlag = false;
                        long moveDownDeniv = lastRegisteredAltitute - newAltituteInt;
                        totalDenivelationDown += moveDownDeniv;
                        if(statistics.currentLapIndex >= 0)
                        {
                            currentLap->lapTotalDenivelationDown += moveDownDeniv;
                            currentLap->lapMinAltitute = min(currentLap->lapMinAltitute, altitute);
                        }
                        tempDenivelationDown += moveDownDeniv;
                        maxTempDenivelationDown = max(maxTempDenivelationDown, tempDenivelationDown);
                        lastRegisteredAltitute = newAltituteInt;
                        setFlagBit(FLAG_INDEX_LAST_REGISTERED_ALTITUTE, true);
                        setFlagBit(FLAG_INDEX_TEMP_DENIVELATION_DOWN, true);
                        setFlagBit(FLAG_INDEX_TOTAL_DENIVELATION_DOWN, true);
                        setFlagBit(FLAG_INDEX_LAP_DENIVELATION_DOWN, true);
                    }
                }
                else // upDownFlag == false
                {
                    if(lastRegisteredAltitute - newAltituteInt >= DENIVELATION_NEEDED_TO_MARK_DM)
                    {
                        long moveDownDeniv = lastRegisteredAltitute - newAltituteInt;
                        totalDenivelationDown += moveDownDeniv;
                        if(statistics.currentLapIndex >= 0)
                        {
                            currentLap->lapTotalDenivelationDown += moveDownDeniv;
                            currentLap->lapMinAltitute = min(currentLap->lapMinAltitute, altitute);
                        }
                        tempDenivelationDown += moveDownDeniv;
                        maxTempDenivelationDown = max(maxTempDenivelationDown, tempDenivelationDown);
                        lastRegisteredAltitute = newAltituteInt;
                        setFlagBit(FLAG_INDEX_LAST_REGISTERED_ALTITUTE, true);
                        setFlagBit(FLAG_INDEX_TEMP_DENIVELATION_DOWN, true);
                        setFlagBit(FLAG_INDEX_TOTAL_DENIVELATION_DOWN, true);
                        setFlagBit(FLAG_INDEX_LAP_DENIVELATION_DOWN, true);
                    }
                    else if(newAltituteInt - lastRegisteredAltitute >= DENIVELATION_NEEDED_TO_MARK_DM)
                    {
                        tempDenivelationUp = 0;
                        tempDenivelationDown = 0;
                        upDownFlag = true;
                        long moveUpDeniv = newAltituteInt - lastRegisteredAltitute;
                        totalDenivelationUp += moveUpDeniv;
                        if(statistics.currentLapIndex >= 0)
                        {
                            currentLap->lapTotalDenivelationUp += moveUpDeniv;
                            currentLap->lapMaxAltitute = max(currentLap->lapMaxAltitute, altitute);
                        }
                        tempDenivelationUp += moveUpDeniv;
                        maxTempDenivelationUp = max(maxTempDenivelationUp, tempDenivelationUp);
                        lastRegisteredAltitute = newAltituteInt;
                        setFlagBit(FLAG_INDEX_LAST_REGISTERED_ALTITUTE, true);
                        setFlagBit(FLAG_INDEX_TEMP_DENIVELATION_UP, true);
                        setFlagBit(FLAG_INDEX_TOTAL_DENIVELATION_UP, true);
                        setFlagBit(FLAG_INDEX_LAP_DENIVELATION_UP, true);
                    }
                }
            }

            if(newAltituteInt < minAltitute) minAltitute = newAltituteInt;
            if(newAltituteInt > maxAltitute) maxAltitute = newAltituteInt;
        }

        if(isFirstMeasure)
        {
            minTemperature = temperature;
            maxTemperature = temperature;
            minHumidity = humidity;
            maxHumidity = humidity;
            minPressure = pressure;
            maxPressure = pressure;
            minAltitute = altitute;
            maxAltitute = altitute;
            lastRegisteredAltitute = altitute;
            
            // mark all flags as changed
            isChangedFlags[0] = 0xFF;
            isChangedFlags[1] = 0xFF;
            isChangedFlags[2] = 0xFF;
        }

        if(isTimeToSaveSensorData()) 
        {
            sumTotalDenivelationWhenSaving = tempDenivelationDown + tempDenivelationUp;
//            eepromWriteCounter = 0;
            saveStatisticsToEEPROM();
//            Sprint(F("counter = "));
//            Sprintln(eepromWriteCounter);
//            Sprintln(F("saved to EEPROM!"));
        }

        if(globalsPressureLogger.isTimeToLogEntry())
        {
            globalsPressureLogger.addEntry(newAltituteInt);
    //        xx++;
     //       globalsPressureLogger.addEntry(xx);
        }

        displayHeaderWrongSensorData(false);
    }
    else
    {
        displayHeaderWrongSensorData(true);
    }

    prevSensorData.temperature = newTemperatureInt;
    prevSensorData.humidity = newHumidityInt;
    prevSensorData.pressure = newPressureInt;
    prevSensorData.altitute = newAltituteInt;
}

void MeasuredValues::resetData()
{
//    displayConfirm(F("reset.."), 7, NULL, 0);

    temperature = INITIAL_DATA;
    humidity = INITIAL_DATA;
    pressure = INITIAL_DATA;
    altitute = INITIAL_DATA;
    prevSensorData.temperature = INITIAL_DATA;
    prevSensorData.humidity = INITIAL_DATA;
    prevSensorData.pressure = INITIAL_DATA;

    upDownFlag = true;
    tempDenivelationUp = 0;
    tempDenivelationDown = 0;
    maxTempDenivelationUp = 0;
    maxTempDenivelationDown = 0;
    totalDenivelationUp = 0;
    totalDenivelationDown = 0;
    minTemperature = INITIAL_DATA;
    maxTemperature = INITIAL_DATA;
    minHumidity = INITIAL_DATA;
    maxHumidity = INITIAL_DATA;
    minPressure = INITIAL_DATA;
    maxPressure = INITIAL_DATA;
    minAltitute = INITIAL_DATA;
    maxAltitute = INITIAL_DATA;

    currentLapIndex = -1;
    for(int i = 0; i < LAP_LOGGER_COUNT; i++)
    {
        lapStatistics[i].lapTotalDenivelationUp = 0;
        lapStatistics[i].lapTotalDenivelationDown = 0;
    }

    lastTimeTemperatureIsMeasuredMs = 0;
    lastTimeSavedtoEEPROM = 0;
    
    sumTotalDenivelationWhenSaving = 0;

    // save to EEPROM
    saveStatisticsToEEPROM();
}


void MeasuredValues::startLap()
{
    /* TODO - calibrate more precise when we start lap
    // adjust the piece of denivelation that has not beed added 
    long moveUpDeniv = altitute - lastRegisteredAltitute;
    if(moveUpDeniv > 0)
    {
        totalDenivelationUp += moveUpDeniv;
//        if(statistics.currentLapIndex >= 0)
    //        currentLap->lapTotalDenivelationUp += moveUpDeniv;
    //        currentLap->lapMaxAltitute = max(currentLap->lapMaxAltitute, altitute);
    tempDenivelationUp += moveUpDeniv;
    maxTempDenivelationUp = max(maxTempDenivelationUp, tempDenivelationUp);

    }
    else
    {
        totalDenivelationDown += -moveUpDeniv;
//        if(statistics.currentLapIndex >= 0)
    //        currentLap->lapTotalDenivelationUp += moveUpDeniv;
    //        currentLap->lapMaxAltitute = max(currentLap->lapMaxAltitute, altitute);
    tempDenivelationUp += moveUpDeniv;
    maxTempDenivelationUp = max(maxTempDenivelationUp, tempDenivelationUp);
    }
    */


    lastRegisteredAltitute = altitute;
    tempDenivelationUp = 0;
    tempDenivelationDown = 0;

    // prepare space for the new lap
    if(currentLapIndex == LAP_LOGGER_COUNT - 1) 
    {
        // move the laps with one position back to make space for a new one at the end
        for(int i = 0; i < LAP_LOGGER_COUNT - 1; i++)
        {
            lapStatistics[i].lapTotalDenivelationUp = lapStatistics[i + 1].lapTotalDenivelationUp;
            lapStatistics[i].lapTotalDenivelationDown = lapStatistics[i + 1].lapTotalDenivelationDown;
            lapStatistics[i].lapMaxAltitute = lapStatistics[i + 1].lapMaxAltitute;
            lapStatistics[i].lapMinAltitute = lapStatistics[i + 1].lapMinAltitute;
        }
        currentLapIndex--; // decrease, to make the logic the same as in other cases
    }

    currentLapIndex++;
    lapStatistics[currentLapIndex].lapTotalDenivelationDown = 0;
    lapStatistics[currentLapIndex].lapTotalDenivelationUp = 0;
    lapStatistics[currentLapIndex].lapMaxAltitute = altitute;
    lapStatistics[currentLapIndex].lapMinAltitute = altitute;
}

// here is how we can use it
bool GetBMECurrentData(float& temperature, float& pressure, float& altitute, float& humidity)
{
    Adafruit_BME280 bme;

    if (!bme.begin()) {
        Sprintln(F("No BME280 sensor, check wiring!"));
        return false;
        while (1);
    }

    temperature = bme.readTemperature();
    pressure = bme.readPressure() / 100.0F;
    altitute = bme.readAltitude(SEALEVELPRESSURE_HPA);
    humidity = bme.readHumidity();
    return true;
}

bool ReadSensorData()
{
    float temperature;
    float pressure;
    float altitute;
    float humidity;

   // long mils = myMillis();
    bool isSensorValid = GetBMECurrentData(temperature, pressure, altitute, humidity);
  //  long mils2 = myMillis();



    if(isSensorValid)
    {
//        Serial.println("---------------------");
//        Serial.print("temperature = ");
//        Serial.println(temperature);
//        Serial.print("pressure = ");
//        Serial.println(pressure);
//        Serial.print("altitute = ");
//        Serial.println(altitute);


        statistics.setNewValues(temperature, pressure, altitute, humidity);
    }

    statistics.lastTimeTemperatureIsMeasuredMs = myMillis();

    Sprint(F("reading sensor..."));
    if(isSensorValid)
        Sprintln(F("OK"));
    else
        Sprintln(F("fail"));

    return isSensorValid;
}


