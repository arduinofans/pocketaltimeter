﻿#include <Wire.h>

#include "libraries\bme280_Drotek\BME280.h"
#include "buttons.h"
#include "StateMachine.h"
#include "customSleep.h"
//#include "timer.h"
#include "measuredValues.h"
#include "pressureLogger.h"


PCD8544 lcd(PIN_SCE, PIN_RESET,PIN_DC,PIN_SDIN,PIN_SCLK,PIN_BLIGHT);

extern GlobalStates globalStates;

void setup() {        

    Serial.begin(115200);
    lcd.begin();

    // init buttons
    pinMode(PIN_button1, INPUT);    
    pinMode(PIN_button4, INPUT);  

    digitalWrite(PIN_button1, HIGH);
    digitalWrite(PIN_button4, HIGH);
    digitalWrite(PIN_button3, HIGH);

    pinMode(PIN_BLIGHT, OUTPUT);  
    digitalWrite(PIN_BLIGHT, HIGH);
    //analogWrite(PIN_BLIGHT, 0);


    bool isInitialized = checkEEPROMIsInitialized();

    initEEPROMData();

    displayON();

    showCenteredMessage(F("Arduino Fans"), 10, 2000);

    if(isInitialized)
    {
        // choose to load or clear the EEPROM values
        readStatisticsFromEEPROM(); // always read - at least for the EEPROM counter to read
        bool loadFromEeeprom = displayConfirm(F("load eeprom?"), 12, NULL, 0, true);
        if(loadFromEeeprom == false){
            statistics.resetData();
        }
    }
    
    // init the sensor data
    ReadSensorData();

    globalStates.currentStateObject->ActivateState();
}


void loop() {
    
    globalButtons.ReadButtonsState();
    
    // MonitorButtons();

    // reset the contrast
    if(globalButtons.allButtonStates == AllButtonState_Btn1_Btn3_Pressed)
    {
        globalButtons.isHandledButtonPress = true;
        setContrast(DEFAULT_SETTING_VALUE_CONTRAST);
        writeSetting(EEPROM_INDEX_CONTRAST, DEFAULT_SETTING_VALUE_CONTRAST);  
        showCenteredMessage(F("reset contrast"), 14, 3500);
    }

    // toggle the backlight
    if(globalButtons.allButtonStates == AllButtonState_Btn1_Btn4_Pressed)
    {
        globalButtons.isHandledButtonPress = true;

        if(readSetting(EEPROM_INDEX_BACKLIGHT) > 0)
        {
            setBacklight(0);
            writeSetting(EEPROM_INDEX_BACKLIGHT, 0);  
        }
        else
        {
            setBacklight(DEFAULT_VALUE_BACKLIGHT_FORCE_ACTIVATION);
            writeSetting(EEPROM_INDEX_BACKLIGHT, DEFAULT_VALUE_BACKLIGHT_FORCE_ACTIVATION);  
        }
        delay(300);
    }

    displayHeaderVoltage();

    displayTimer();
    
    // read current sensor data
    if(statistics.isTimeToReadSensorData())
    {
       bool isSensorValid = ReadSensorData();
       if(isSensorValid == false)
       {
           if(globalStates.currentState != state_message_box)
           {
               // discovering that the sensor is missing
               globalStates.messageBoxState.setMessage( F("no sensor !!!"));
               globalStates.setNewState(state_message_box);
           }
           else { /* do nothing - there is a message already displayed */}
       }
       else 
       {
           if(globalStates.currentState == state_message_box)
           {
               // the sensor has been just connected
               globalStates.setNewState(globalStates.previousState);
           }
           else // normal work
           {
               // do not display this when we are in settings or summary screens
               if(globalStates.currentState != state_setttings && globalStates.currentState != state_summary
                   && globalStates.currentState != state_lap_summary && globalSettings.settingsValue_ReadSensorPeriodS > 1)
               {
                   showCenteredMessage(F("reading..."), 5);
                   delay(30);
                   // lcd.gotoXY(6 * 13, 0);
                    //lcd.print(F("R"));
                    //delay(50);
                    //lcd.gotoXY(6 * 13, 0);
                    //lcd.print(F(" "));
               }
           }
       }
    }


    // here is the main logic for each state
    globalStates.currentStateObject->PerformAction();

    delay(globalStates.currentStateObject->sleepTimeInMs);

    bool isGoToDeepSleep = myMillis() - globalButtons.working_time_ms_for_deep_sleep > GOTO_DEEP_SLEEP_AFTER_IDLE_S * 1000;
    if( isGoToDeepSleep)
    {
        // prevent deep sleep so far
//        int deepSleepPeriod = globalStates.currentStateObject->deepSleepTimeInS;
//        if( deepSleepPeriod > 0)
//        {
//            Sprintln(globalStates.currentStateObject->sleepTimeInMs);
            gotoDeepSleep(0xFFFF); //(deepSleepPeriod);
//        }
    }
}
