#ifndef DISPLAY_H
#define DISPLAY_H

#include "Arduino.h"
#include "libraries\PCD8544\PCD8544.h"
#include "constants.h"
#include "common.h"
#include "measuredValues.h"
#include "eepromHelper.h"

extern MeasuredValues statistics;
extern GlobalSettings globalSettings;
extern PCD8544 lcd;

// common LED display functions
void setContrast(byte value);
void setBacklight(byte value);
void displayOFF();
void displayON();

// common functions for displaying numbers to the display
void fillFloatAsIntToTakeExactSpace(long number, byte sizeToTake, byte decimals);
void fillLongToTakeExactSpace(long number, byte sizeToTake);
void fillIntToTakeExactSpace(int number, byte sizeToTake);
byte lengthOfIntNumber(long number);
void printEmptySpaces(int countSpaces);
void displayFloatIntInExactSpace(long floatIntValue, byte spaceToTake, byte decimals = 1);
bool displayConfirm(const __FlashStringHelper* newMessage1, byte length1, const __FlashStringHelper* newMessage2, byte length2, bool defaultResult = false);
bool displayConfirm(int number, byte length1, bool defaultResult = false);

// functions for the display header
void initTimer();
void displayTimer();
void displayHeaderSleepState(bool isSleeping);
void displayHeaderWrongSensorData(bool isWrong);
void displayHeaderVoltage();

//  common general functions
void showCenteredMessage(const __FlashStringHelper* newMessage, byte length, int delayPeriodMs = 300);
void displayMessageBox(__FlashStringHelper* message);

// Normal State
void displayNormalScreen(bool forceDisplayRedraw);

// Tracking State
void displayTrackingScreen(bool forceDisplayRedraw);

// Summary screen 1
void displaySummaryScreen1(bool forceDisplayRedraw);

// Summary screen 2
void displaySummaryScreen2(bool forceDisplayRedraw);

// Summary screen 3
void displaySummaryScreen3(bool forceDisplayRedraw);

// Summary screen 4
void displaySummaryScreen4(bool forceDisplayRedraw);

// Settings1 - SetContrast
void displaySettingsScreenSetContrast(bool forceDisplayRedraw, byte currentValue);

// Settings2 - TurnOffDisplayAfter
void displaySettingsScreenTurnOffDisplayAfter(bool forceDisplayRedraw, byte currentValue);

// Settings3 - Read Sensor Period screen
void displaySettingsScreenReadSensorPeriod(bool forceDisplayRedraw, byte currentValue);

// Settings4 - Reset all
void displaySettingsScreenResetAll(bool forceDisplayRedraw, unsigned long resetActivationTime);

// Settings5 - Interpolate Altitute
void displaySettingsScreenInterpolateAltitute(bool forceDisplayRedraw, int currentValue);

// Settings - Interpolate Altitute
void displaySettingsScreenCalibrateAltitute(bool forceDisplayRedraw, int currentValue, bool flagCalibrationDirection);

// Settings5 - Backlight
void displaySettingsBacklight(bool forceDisplayRedraw, byte currentValue);

// Lap Summary 
void displayLapSummaryScreen(bool forceDisplayRedraw, byte currentLapToView, byte totalLaps);

#endif // DISPLAY_H
