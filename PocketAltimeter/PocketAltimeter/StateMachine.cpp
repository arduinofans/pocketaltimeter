#include"StateMachine.h"

extern GlobalButtons globalButtons;

GlobalStates globalStates;

void BaseState::ActivateState() 
{ 
    activationTime = myMillis(); 
    initTimer();
};


NormalState::NormalState()
{
    BaseState::sleepTimeInMs = NORMAL_STATE_SLEEP_TIME_MS;
//    BaseState::deepSleepTimeInS = NORMAL_STATE_DEEP_SLEEP_TIME_S;
}
void NormalState::ActivateState()
{ 
    showCenteredMessage(F("normal"), 6);
    BaseState::ActivateState();
}
void NormalState::PerformAction()
{
    if(globalButtons.isHandledButtonPress == false)
    {
        if(globalButtons.allButtonStates == AllButtonState_Btn1_ShortReleased)
        {
            // mark the button event as consumed 
            globalButtons.isHandledButtonPress = true;
            if(globalStates.isDisplayActive == false)
            {
                globalStates.turnOnDisplay();
            }
            else
            {
                globalStates.setNewState(state_tracking);
            }
        }
        else if(globalButtons.allButtonStates == AllButtonState_Btn1_LongPressed)
        {
            // mark the button event as consumed 
            globalButtons.isHandledButtonPress = true;
            globalStates.ToggleDisplay();
        }
        else if(globalButtons.allButtonStates == AllButtonState_Btn4_ShortReleased)
        {
            // mark the button event as consumed 
            globalButtons.isHandledButtonPress = true;
            if(globalStates.isDisplayActive == false)
            {
                globalStates.turnOnDisplay();
            }
            else
            {
                globalStates.setNewState(state_summary);
            }
        }
        else if(globalButtons.allButtonStates == AllButtonState_Btn4_LongPressed)
        {
            // mark the button event as consumed 
            globalButtons.isHandledButtonPress = true;
            globalStates.turnOnDisplay();   // light up the display, just in case
            globalStates.setNewState(state_setttings);
        }
        else if(globalButtons.allButtonStates == AllButtonState_Btn3_Btn4_Pressed)
        {
            // mark the button event as consumed 
            globalButtons.isHandledButtonPress = true;
            if(globalSettings.canWriteToEEPROM)
            {
                writeSetting(EEPROM_INDEX_CAN_WRITE_TO_EEPROM, false);
                showCenteredMessage(F("EEPROM: Off"), 11);
            }
            else
            {
                writeSetting(EEPROM_INDEX_CAN_WRITE_TO_EEPROM, true);
                showCenteredMessage(F("EEPROM: On"), 10);
            }
        }
    }
    else if(globalStates.isDisplayActive 
        && (myMillis() - globalButtons.working_time_ms >= (long)1000*60*globalSettings.settingsValue_TurnOffDisplayAfterMin))
    {        
        globalStates.turnOffDisplay();
    }
    else if(globalStates.isDisplayActive)   // display the info if the screen is active
    {
        displayNormalScreen(globalStates.forceDisplayRedraw);
        globalStates.forceDisplayRedraw = false;
    }
}

TrackingState::TrackingState()
{
    BaseState::sleepTimeInMs = TRACKING_STATE_SLEEP_TIME_MS;
//    BaseState::deepSleepTimeInS = TRACKING_STATE_DEEP_SLEEP_TIME_S;
}
void TrackingState::ActivateState()
{ 
    showCenteredMessage(F("tracking"), 8);
    BaseState::ActivateState();
}
void TrackingState::PerformAction()
{
    if(globalButtons.isHandledButtonPress == false)
    {
        if(globalButtons.allButtonStates == AllButtonState_Btn1_ShortReleased)
        {
            // mark the button event as consumed 
            globalButtons.isHandledButtonPress = true;
            if(globalStates.isDisplayActive == false)
            {
                globalStates.turnOnDisplay();
            }
            else
            {
                globalStates.setNewState(state_normal);
            }
        }
        else if(globalButtons.allButtonStates == AllButtonState_Btn1_LongPressed)
        {
            // mark the button event as consumed 
            globalButtons.isHandledButtonPress = true;
            globalStates.ToggleDisplay();
        }
        else if(globalButtons.allButtonStates == AllButtonState_Btn4_ShortReleased)
        {
            // mark the button event as consumed 
            globalButtons.isHandledButtonPress = true;
            if(globalStates.isDisplayActive == false)
            {
                globalStates.turnOnDisplay();
            }
            else
            {
                globalStates.setNewState(state_summary);
            }
        }
        else if(globalButtons.allButtonStates == AllButtonState_Btn3_ShortReleased)
        {
            globalButtons.isHandledButtonPress = true;
            // trendo
//            Serial.println((int)statistics.currentLapIndex);
            if(statistics.currentLapIndex >= 0)
            {
                globalStates.setNewState(state_lap_summary);
            }
        }
        else if(globalButtons.allButtonStates == AllButtonState_Btn3_LongPressed)
        {
            globalButtons.isHandledButtonPress = true;
            bool startLap = displayConfirm(F("Start lap?"), 10, NULL, 0, false);
            if(startLap)
            {
                statistics.startLap();
            }
        }
        else if(globalButtons.allButtonStates == AllButtonState_Btn4_LongPressed)
        {
            // mark the button event as consumed 
            globalButtons.isHandledButtonPress = true;
            globalStates.turnOnDisplay();   // light up the display, just in case
            globalStates.setNewState(state_setttings);
        }
        else if(globalButtons.allButtonStates == AllButtonState_Btn3_Btn4_Pressed)
        {
            // mark the button event as consumed 
            globalButtons.isHandledButtonPress = true;
            if(globalSettings.canWriteToEEPROM)
            {
                writeSetting(EEPROM_INDEX_CAN_WRITE_TO_EEPROM, false);
                showCenteredMessage(F("EEPROM: Off"), 11);
            }
            else
            {
                writeSetting(EEPROM_INDEX_CAN_WRITE_TO_EEPROM, true);
                showCenteredMessage(F("EEPROM: On"), 10);
            }
        }
    }
    else if(globalStates.isDisplayActive 
        && (myMillis() - globalButtons.working_time_ms >= (long)1000*60*globalSettings.settingsValue_TurnOffDisplayAfterMin))
    {
        globalStates.turnOffDisplay();
    }
    else if(globalStates.isDisplayActive)   // display the info if the screen is active
    {
        displayTrackingScreen(globalStates.forceDisplayRedraw);
        globalStates.forceDisplayRedraw = false;
    }
//    Sprintln(myMillis() - globalButtons.working_time_ms);
//    delay(100);
}

SettingsState::SettingsState()
{
    BaseState::sleepTimeInMs = SETTINGS_STATE_SLEEP_TIME_MS;
//    BaseState::deepSleepTimeInS = SETTINGS_STATE_DEEP_SLEEP_TIME_S;
    current_screen = settings_screen_set_contrast;
    flagCalibrationDirection = true;
//        readCurrentSettingValue();
}
void SettingsState::ActivateState()
{ 
    BaseState::ActivateState();
    readCurrentSettingValue();
    resetActivationTime = 0; // clear the reset flag
}
void SettingsState::gotoNextSetting()
{
    switch(current_screen)
    {
        case settings_screen_set_contrast:
            current_screen = settings_screen_turn_off_display_after_min;
        break;
        case settings_screen_turn_off_display_after_min:
            current_screen = settings_screen_read_sensor_every_s;
        break;
        case settings_screen_read_sensor_every_s:
            current_screen = settings_screen_reset_all;
        break;
        case settings_screen_reset_all:
            resetActivationTime = 0; // clear the reset flag
            current_screen = settings_screen_interpolate_altitute;
        break;
        case settings_screen_interpolate_altitute:
            current_screen = settings_screen_calibrate_altitute;
        break;
        case settings_screen_calibrate_altitute:
            current_screen = settings_screen_backlight;
        break;
        case settings_screen_backlight:
            current_screen = settings_screen_set_contrast;
        break;
    }

    globalStates.forceDisplayRedraw = true; // not sure if this is the best way to update the screen

    Sprint(F("Entering setting screen #"));
    Sprintln(current_screen);
    readCurrentSettingValue();
}
void SettingsState::readCurrentSettingValue()
{
    switch(current_screen)
    {
        case settings_screen_set_contrast:
            currentScreenSettingValue = globalSettings.settingsValue_Contrast;
        break;
        case settings_screen_turn_off_display_after_min:
            currentScreenSettingValue = globalSettings.settingsValue_TurnOffDisplayAfterMin;
        break;
        case settings_screen_read_sensor_every_s:
            currentScreenSettingValue = globalSettings.settingsValue_ReadSensorPeriodS;
        break;
        case settings_screen_reset_all:
            // no value here
        break;
        case settings_screen_interpolate_altitute:
            currentScreenSettingValue = globalSettings.settingsValue_InterpolateAltitute;
        break;
        case settings_screen_calibrate_altitute:
            currentScreenSettingValue2Bytes = globalSettings.settingsValue_CalibrateAltituteMeters;
        break;
        case settings_screen_backlight:
            currentScreenSettingValue = globalSettings.settingsValue_backlight;
        break;

    }
}
void SettingsState::writeCurrentSettingValue(byte newValue)
{
    switch(current_screen)
    {
        case settings_screen_set_contrast:
            writeSetting(EEPROM_INDEX_CONTRAST, newValue);                
        break;
        case settings_screen_turn_off_display_after_min:
            writeSetting(EEPROM_INDEX_TURN_OFF_DISPLAY_AFTER_MIN, newValue);
        break;
        case settings_screen_read_sensor_every_s:
            writeSetting(EEPROM_INDEX_READ_SENSOR_PERIOD_S, newValue);    
        break;
        case settings_screen_reset_all:
            // no value here
        break;
        case settings_screen_interpolate_altitute:
            writeSetting(EEPROM_INDEX_INTERPOLATE_ALTITUTE, newValue);    
        break;
        case settings_screen_calibrate_altitute:
            writeSetting(EEPROM_INDEX_CALIBRATE_ALTITUTE_M, newValue);    
        break;
        case settings_screen_backlight:
            writeSetting(EEPROM_INDEX_BACKLIGHT, newValue);    
        break;
    }
}
void SettingsState::writeCurrentSettingValue2Bytes(int newValue)
{
    switch(current_screen)
    {
        case settings_screen_calibrate_altitute:
            writeSetting2Bytes(EEPROM_INDEX_CALIBRATE_ALTITUTE_M, newValue);    
        break;
    }
}

// not used so far - does not save a lot memory
byte getNext(byte currValue, byte arr[], byte arrSize)
{
    byte nextValue;
    byte i;
    byte nextIndex;

    nextValue = arr[0];
    for(i=0; i < arrSize && arr[i] != currValue; i++)
    {}
    nextIndex = i+1;
    if(nextIndex < arrSize)
    {
        nextValue = arr[nextIndex];
    }
    return nextValue;
}

// param - optional parameter; so far is used to set calibration denivalation steps
void SettingsState::findNextSettingsValue(int param) 
{
    byte settings_values_contrast[]                     = SETTINGS_ARRAY_CONTRAST
    byte settings_values_turn_off_display_after_min[]   = SETTINGS_ARRAY_TURN_OFF_DISPLAY_AFTER_MIN
    byte settings_values_read_sensor_period_s[]         = SETTINGS_ARRAY_READ_SENSOR_PERIOD_S
    byte settings_values_interpolate_altitute[]         = SETTINGS_ARRAY_INTERPOLATE_ALTITUTE
    byte settings_values_backlight[]                    = SETTINGS_ARRAY_BACKLIGHT_PERCENTAGE

    byte nextValue;
    int nextIntValue;
    byte i;
    byte nextIndex;
    
    switch(current_screen)
    {
        case settings_screen_set_contrast:
      //      nextValue = getNext(currentScreenSettingValue, settings_values_contrast, ARRAY_SIZE(settings_values_contrast));
            nextValue = settings_values_contrast[0];
            for(i=0; i < ARRAY_SIZE(settings_values_contrast) && settings_values_contrast[i] != currentScreenSettingValue; i++)
            {}
            nextIndex = i+1;
            if(nextIndex < ARRAY_SIZE(settings_values_contrast))
            {
                nextValue=settings_values_contrast[nextIndex];
            }
            
            setContrast(nextValue);
        break;
        case settings_screen_turn_off_display_after_min:
       //     nextValue = getNext(currentScreenSettingValue, settings_values_turn_off_display_after_min, ARRAY_SIZE(settings_values_turn_off_display_after_min));
            
            nextValue = settings_values_turn_off_display_after_min[0];
            for(i=0; i < ARRAY_SIZE(settings_values_turn_off_display_after_min) && settings_values_turn_off_display_after_min[i] != currentScreenSettingValue; i++)
            {}
            nextIndex = i+1;
            if(nextIndex < ARRAY_SIZE(settings_values_turn_off_display_after_min))
            {
                nextValue=settings_values_turn_off_display_after_min[nextIndex];
            }
            
        break;
        case settings_screen_read_sensor_every_s:
        //    nextValue = getNext(currentScreenSettingValue, settings_values_read_sensor_period_s, ARRAY_SIZE(settings_values_read_sensor_period_s));
            
            nextValue = settings_values_read_sensor_period_s[0];
            for(i=0; i < ARRAY_SIZE(settings_values_read_sensor_period_s) && settings_values_read_sensor_period_s[i] != currentScreenSettingValue; i++)
            {}
            nextIndex = i+1;
            if(nextIndex < ARRAY_SIZE(settings_values_read_sensor_period_s))
            {
                nextValue=settings_values_read_sensor_period_s[nextIndex];
            }
            
        break;
        case settings_screen_reset_all:
            // no value here
        break;

        case settings_screen_interpolate_altitute:
        //    nextValue = 
            nextValue = settings_values_interpolate_altitute[0];
            for(i=0; i < ARRAY_SIZE(settings_values_interpolate_altitute) && settings_values_interpolate_altitute[i] != currentScreenSettingValue; i++)
            {}
            nextIndex = i+1;
            if(nextIndex < ARRAY_SIZE(settings_values_interpolate_altitute))
            {
                nextValue=settings_values_interpolate_altitute[nextIndex];
            }
        break;
            
        case settings_screen_calibrate_altitute:
            if(flagCalibrationDirection == true)
                nextIntValue = globalSettings.settingsValue_CalibrateAltituteMeters + param;
            else
                nextIntValue = globalSettings.settingsValue_CalibrateAltituteMeters - param;
            // it is treated differently to save writing to EEPROM and save it when exit the screen
            globalSettings.settingsValue_CalibrateAltituteMeters = nextIntValue;
            currentScreenSettingValue2Bytes = nextIntValue;
            writeCurrentSettingValue2Bytes(nextIntValue);
            return; // exit because this is 2 bytes, not one !
        break;

        case settings_screen_backlight:
        //    nextValue = 
            nextValue = settings_values_backlight[0];
            for(i=0; i < ARRAY_SIZE(settings_values_backlight) && settings_values_backlight[i] != currentScreenSettingValue; i++)
            {}
            nextIndex = i+1;
            if(nextIndex < ARRAY_SIZE(settings_values_backlight))
            {
                nextValue=settings_values_backlight[nextIndex];
            }
            setBacklight(nextValue);
        break;
    }

    // todo - may be in the future do not save it this way
    writeCurrentSettingValue(nextValue);

  //new  globalStates.forceDisplayRedraw = true; // not sure if this is the best way to update the screen

    currentScreenSettingValue = nextValue;
//    Sprint(F("    Next value: "));
//    Sprintln(nextValue);
}
void SettingsState::PerformAction()
{
    if(globalButtons.isHandledButtonPress == false)
    {
        if(globalButtons.allButtonStates == AllButtonState_Btn1_ShortReleased)
        {
            // save the altitute calibration if this is the screen
            if(current_screen == settings_screen_calibrate_altitute)
            {
                writeSetting2Bytes(EEPROM_INDEX_CALIBRATE_ALTITUTE_M, globalSettings.settingsValue_CalibrateAltituteMeters);
            }
            // mark the button event as consumed 
            globalButtons.isHandledButtonPress = true;
            globalStates.setNewState(globalStates.previousState);
        }
        switch(current_screen)
        {
            case settings_screen_set_contrast:
//                if(globalButtons.allButtonStates == AllButtonState_Btn1_ShortReleased)
    //            {
        //        }
                if(globalButtons.allButtonStates == AllButtonState_Btn3_ShortReleased)
                {
                    // mark the button event as consumed 
                    globalButtons.isHandledButtonPress = true;
                    this->findNextSettingsValue();
                }
                else if(globalButtons.allButtonStates == AllButtonState_Btn4_ShortReleased)
                {
                    // mark the button event as consumed 
                    globalButtons.isHandledButtonPress = true;
                    this->gotoNextSetting();
                }
            break;

            case settings_screen_turn_off_display_after_min:
                if(globalButtons.allButtonStates == AllButtonState_Btn3_ShortReleased)
                {
                    // mark the button event as consumed 
                    globalButtons.isHandledButtonPress = true;
                    this->findNextSettingsValue();
                }
                else if(globalButtons.allButtonStates == AllButtonState_Btn4_ShortReleased)
                {
                    // mark the button event as consumed 
                    globalButtons.isHandledButtonPress = true;
                    this->gotoNextSetting();
                }
            break;

            case settings_screen_read_sensor_every_s:
                if(globalButtons.allButtonStates == AllButtonState_Btn3_ShortReleased)
                {
                    // mark the button event as consumed 
                    globalButtons.isHandledButtonPress = true;
                    this->findNextSettingsValue();
                }
                else if(globalButtons.allButtonStates == AllButtonState_Btn4_ShortReleased)
                {
                    // mark the button event as consumed 
                    globalButtons.isHandledButtonPress = true;
                    this->gotoNextSetting();
                }
            break;

            case settings_screen_reset_all:
                if(globalButtons.allButtonStates == AllButtonState_Btn3_ShortReleased)
                {
                    // active the reset countdown
                        resetActivationTime = millis(); // no need to be myMillis()
                        // mark the button event as consumed 
                        globalButtons.isHandledButtonPress = true;
                }
                else if(globalButtons.allButtonStates == AllButtonState_Btn4_ShortReleased)
                {
                    // mark the button event as consumed 
                    globalButtons.isHandledButtonPress = true;
                    this->gotoNextSetting();
                }
            break;

            case settings_screen_interpolate_altitute:
                if(globalButtons.allButtonStates == AllButtonState_Btn3_ShortReleased)
                {
                    // mark the button event as consumed 
                    globalButtons.isHandledButtonPress = true;
                    this->findNextSettingsValue();
                }
                else if(globalButtons.allButtonStates == AllButtonState_Btn4_ShortReleased)
                {
                    // mark the button event as consumed 
                    globalButtons.isHandledButtonPress = true;
                    this->gotoNextSetting();
                }
            break;

            case settings_screen_calibrate_altitute:
                if(globalButtons.allButtonStates == AllButtonState_Btn3_ShortReleased)
                {
                    // mark the button event as consumed 
                    globalButtons.isHandledButtonPress = true;
                    this->findNextSettingsValue(1);
                }
                else if(globalButtons.allButtonStates == AllButtonState_Btn3_LongPressed)
                {
                    // mark the button event as consumed 
                    globalButtons.isHandledButtonPress = true;
                    this->findNextSettingsValue(10);
                }
                else if(globalButtons.allButtonStates == AllButtonState_Btn4_ShortReleased)
                {
                    // save the altitute calibration if this is the screen
                    writeSetting2Bytes(EEPROM_INDEX_CALIBRATE_ALTITUTE_M, globalSettings.settingsValue_CalibrateAltituteMeters);

                    // mark the button event as consumed 
                    globalButtons.isHandledButtonPress = true;
                    this->gotoNextSetting();
                }
                else if(globalButtons.allButtonStates == AllButtonState_Btn4_LongPressed)
                {
                    // mark the button event as consumed 
                    globalButtons.isHandledButtonPress = true;
                    flagCalibrationDirection = !flagCalibrationDirection;
                    globalStates.forceDisplayRedraw = true;
                }
            break;


            case settings_screen_backlight:
                if(globalButtons.allButtonStates == AllButtonState_Btn3_ShortReleased)
                {
                    // mark the button event as consumed 
                    globalButtons.isHandledButtonPress = true;
                    this->findNextSettingsValue();
                }
                else if(globalButtons.allButtonStates == AllButtonState_Btn4_ShortReleased)
                {
                    // mark the button event as consumed 
                    globalButtons.isHandledButtonPress = true;
                    this->gotoNextSetting();
                }
            break;
        }
    }
    else if(myMillis() - globalButtons.working_time_ms > (long)1000 * SETTINGS_SCREEN_LEAVE_TIMEOUT_S)
    {
        // save the altitute calibration if this is the screen
        if(current_screen == settings_screen_calibrate_altitute)
        {
            writeSetting2Bytes(EEPROM_INDEX_CALIBRATE_ALTITUTE_M, globalSettings.settingsValue_CalibrateAltituteMeters);
        }
        globalStates.setNewState(globalStates.previousState);
    }
    else if(globalStates.isDisplayActive)   // display the info if the screen is active
    {
        switch(current_screen)
        {
            case settings_screen_set_contrast:
                displaySettingsScreenSetContrast(globalStates.forceDisplayRedraw, currentScreenSettingValue);
                globalStates.forceDisplayRedraw = false;
                break;
            case settings_screen_turn_off_display_after_min:
                displaySettingsScreenTurnOffDisplayAfter(globalStates.forceDisplayRedraw, currentScreenSettingValue);
                globalStates.forceDisplayRedraw = false;
                break;
            case settings_screen_read_sensor_every_s:
                displaySettingsScreenReadSensorPeriod(globalStates.forceDisplayRedraw, currentScreenSettingValue);
                globalStates.forceDisplayRedraw = false;
                break;
            case settings_screen_reset_all:
                if(resetActivationTime != 0 && millis() - resetActivationTime >= 5000) // we do not need myMillis() here
                {
                    statistics.resetData();
                    Sprintln(F("statistics are reset !!"));
                    globalStates.setNewState(globalStates.previousState);
                }
                else
                {
                    displaySettingsScreenResetAll(globalStates.forceDisplayRedraw, resetActivationTime);
                    globalStates.forceDisplayRedraw = false;
                }
                break;
            case settings_screen_interpolate_altitute:
                displaySettingsScreenInterpolateAltitute(globalStates.forceDisplayRedraw, currentScreenSettingValue);
                globalStates.forceDisplayRedraw = false;
                break;
            case settings_screen_calibrate_altitute:
                displaySettingsScreenCalibrateAltitute(globalStates.forceDisplayRedraw, currentScreenSettingValue2Bytes, flagCalibrationDirection);
                globalStates.forceDisplayRedraw = false;
                break;
            case settings_screen_backlight:
                displaySettingsBacklight(globalStates.forceDisplayRedraw, currentScreenSettingValue);
                globalStates.forceDisplayRedraw = false;
                break;

        }
    }
}

SummaryState::SummaryState()
{
    BaseState::sleepTimeInMs = SUMMARY_STATE_SLEEP_TIME_MS;
//    BaseState::deepSleepTimeInS = SUMMARY_STATE_DEEP_SLEEP_TIME_S;
    current_screen = summary_screen_1;
}
void SummaryState::ActivateState()
{ 
    BaseState::ActivateState();
}
void SummaryState::gotoNextSummary()
{
    switch(current_screen)
    {
        case summary_screen_1:
            current_screen = summary_screen_2;
        break;
        case summary_screen_2:
            current_screen = summary_screen_3;
        break;
        case summary_screen_3:
            current_screen = summary_screen_4;
        break;
        case summary_screen_4:
            current_screen = summary_screen_1;
        break;
    }

    globalStates.forceDisplayRedraw = true; // not sure if this is the best way to update the screen

    Sprint(F("Entering summay screen #"));
    Sprintln(current_screen);
}
void SummaryState::PerformAction()
{
    if(globalButtons.isHandledButtonPress == false)
    {
        if(globalButtons.allButtonStates == AllButtonState_Btn1_ShortReleased)
        {
            // mark the button event as consumed 
            globalButtons.isHandledButtonPress = true;
            globalStates.setNewState(globalStates.previousState);
        }
        else if(globalButtons.allButtonStates == AllButtonState_Btn4_ShortReleased)
        {
            // mark the button event as consumed 
            globalButtons.isHandledButtonPress = true;
            this->gotoNextSummary();
        }
        else if(globalButtons.allButtonStates == AllButtonState_Btn4_LongPressed)
        {
            // mark the button event as consumed 
            globalButtons.isHandledButtonPress = true;
            globalStates.setNewState(state_setttings);
        }
    }
    else if(myMillis() - globalButtons.working_time_ms > (long)1000 * SUMMARY_SCREEN_LEAVE_TIMEOUT_S)
    {
        globalStates.setNewState(globalStates.previousState);
    }
    else if(globalStates.isDisplayActive)   // display the info if the screen is active
    {
        switch(current_screen)
        {
            case summary_screen_1:
                displaySummaryScreen1(globalStates.forceDisplayRedraw);
                globalStates.forceDisplayRedraw = false;
                break;
            case summary_screen_2:
                displaySummaryScreen2(globalStates.forceDisplayRedraw);
                globalStates.forceDisplayRedraw = false;
                break;
            case summary_screen_3:
                displaySummaryScreen3(globalStates.forceDisplayRedraw);
                globalStates.forceDisplayRedraw = false;
                break;
            case summary_screen_4:
                displaySummaryScreen4(globalStates.forceDisplayRedraw);
                globalStates.forceDisplayRedraw = false;
                break;
        }
    }

}


LapSummaryState::LapSummaryState()
{
    BaseState::sleepTimeInMs = LAP_SUMMARY_STATE_SLEEP_TIME_MS;
}
void LapSummaryState::ActivateState()
{ 
    currentLapToView = (byte) statistics.currentLapIndex;
    BaseState::ActivateState();
}
void LapSummaryState::gotoNextSummary()
{
    // move 1 position back, or if negative, move at the end
    if(currentLapToView == 0)
    {
        currentLapToView = statistics.currentLapIndex;
    }
    else
    {
        currentLapToView--;
    }

    globalStates.forceDisplayRedraw = true; // not sure if this is the best way to update the screen

//    Sprint(F("Entering lap summay screen #"));
//    Sprintln(current_screen);
}
void LapSummaryState::PerformAction()
{
    if(globalButtons.isHandledButtonPress == false)
    {
        if(globalButtons.allButtonStates == AllButtonState_Btn1_ShortReleased
            || globalButtons.allButtonStates == AllButtonState_Btn4_ShortReleased)
        {
            // mark the button event as consumed 
            globalButtons.isHandledButtonPress = true;
            globalStates.setNewState(globalStates.previousState);
        }
        else if(globalButtons.allButtonStates == AllButtonState_Btn3_ShortReleased)
        {
            // mark the button event as consumed 
            globalButtons.isHandledButtonPress = true;
            gotoNextSummary();
        }
    }
    else if(myMillis() - globalButtons.working_time_ms > (long)1000 * SUMMARY_SCREEN_LEAVE_TIMEOUT_S)
    {
        globalStates.setNewState(globalStates.previousState);
    }
    else if(globalStates.isDisplayActive)   // display the info if the screen is active
    {
        displayLapSummaryScreen(globalStates.forceDisplayRedraw, currentLapToView, (byte) (statistics.currentLapIndex + 1)); 
        globalStates.forceDisplayRedraw = false;
    }

}

MessageBoxState::MessageBoxState()
{
    BaseState::sleepTimeInMs = MESSAGE_BOX_STATE_SLEEP_TIME_MS;
//    BaseState::deepSleepTimeInS = MESSAGE_BOX_STATE_DEEP_SLEEP_TIME_S;
    message = (__FlashStringHelper*)F("empty");
}
void MessageBoxState::setMessage(const __FlashStringHelper* newMessage)
{
    message = (__FlashStringHelper*)newMessage;
}
void MessageBoxState::PerformAction()
{
    if(globalButtons.isHandledButtonPress == false)
    {
        if(globalButtons.allButtonStates == AllButtonState_Btn1_ShortReleased)
        {
            // mark the button event as consumed 
            globalButtons.isHandledButtonPress = true;
            globalStates.setNewState(globalStates.previousState);
        }
    }
}
void MessageBoxState::ActivateState()
{ 
    BaseState::ActivateState();
    if(globalStates.isDisplayActive)   // display the info if the screen is active
    {
        displayMessageBox(this->message);
    }
};

GlobalStates::GlobalStates()
{
    init();
}
void GlobalStates::init()
{
    currentState = state_normal;
    currentStateObject = &normalState;
    previousState = state_normal;
    isDisplayActive = true;
    forceDisplayRedraw = true;

    lastTimeBatteryCheckMs = 0;

    lastSec = 0;
    lastMin = 0;
    lastHour = 0;
    lastDay = 0;
}
void GlobalStates::setNewState(states newState)
{
    if(currentState == state_normal || currentState == state_tracking)
    {
        previousState = currentState;
    }

    switch (newState)
    {
    case state_normal:
        currentStateObject = &normalState;
        currentState = state_normal;
        Sprintln(F("Entering: normalState"));
        break;
    case state_tracking:
        currentStateObject = &trackingState;
        currentState = state_tracking;
        Sprintln(F("Entering: trackingState"));
        break;
    case state_setttings:
        currentStateObject = &settingsState;
        currentState = state_setttings;
        Sprintln(F("Entering: settingsState"));
        break;
    case state_summary:
        currentStateObject = &summaryState;
        currentState = state_summary;
        Sprintln(F("Entering: summaryState"));
        break;
    case state_message_box:
        currentStateObject = &messageBoxState;
        currentState = state_message_box;
        Sprintln(F("Entering: messageBoxState"));
        break;
    case state_lap_summary:
        currentStateObject = &lapSummaryState;
        currentState = state_lap_summary;
        Sprintln(F("Entering: lapSummaryState"));
        break;
    }

    forceDisplayRedraw = true;

    // todo - improve if it is slow
    // todo - should we active it here?
    currentStateObject->ActivateState();
}
void GlobalStates::ToggleDisplay()
{
    if(isDisplayActive)
    {
        turnOffDisplay();
    }
    else
    {
        turnOnDisplay();
    }
}
void GlobalStates::turnOffDisplay()
{
    isDisplayActive = false;
    displayOFF();
    Sprintln(F("Turn off display"));
    delay(10);
}
void GlobalStates::turnOnDisplay()
{
    isDisplayActive = true;
    forceDisplayRedraw = true;
    displayON();
    Sprintln(F("Turn on display"));
    delay(10);
}
