#include "Arduino.h"
#include "libraries\PCD8544\PCD8544.h"
#include "libraries\Arduino_Vcc\Vcc.h"
#include "constants.h"
#include "common.h"
#include "measuredValues.h"
#include "StateMachine.h"
#include "eepromHelper.h"
#include "pressureLogger.h"

//extern MeasuredValues statistics;
extern PCD8544 lcd;
extern GlobalSettings globalSettings;
extern GlobalStates globalStates;
extern GlobalButtons globalButtons;
extern PressureLogger globalsPressureLogger;

#define OFFSET_LEFT                     ((byte) 1)
#define SYMBOL_SIZE                     ((byte) 6)
#define DISPLAY_SYMBOLS_PER_ROW         ((byte) 14)
#define SETTINGS_BLINK_PERIOD           ((long) 500)
#define SUMMARY_BLINK_PERIOD            ((long) 2000)

#define CONFIRM_WAIT_MAX_PERIOD_MS      ((long) 6000)

static bool _blinkFlag = false; // a variable used for the setting screen to blink current item; it is used to detect change

// common LED display functions
void setContrast(byte value)
{
    lcd.setContrast(value);
}
void setBacklight(byte backlightPercentage)
{
    byte byteValue = (100 - backlightPercentage) * 2.55;
    analogWrite(PIN_BLIGHT, byteValue);
}
void displayOFF()
{
    setBacklight(0);
    lcd.setPower(false);
}
void displayON()
{
    lcd.setPower(true);
    setContrast(globalSettings.settingsValue_Contrast);
    setBacklight(globalSettings.settingsValue_backlight);

    //  lcd.setContrast(60); // 70 for 5v
}

// common functions for displaying numbers to the display
void fillFloatAsIntToTakeExactSpace(long number, byte sizeToTake, byte decimals)
{
    // the min size for each number is 3 positions!
    // for negative numbers - 4 positions !

    byte positionsCount = 0;
    if(decimals > 0)
    {
        positionsCount = decimals + 1;    // the dot and symbols after the dot
    }
    if(number < 0) 
    {
        positionsCount++;        // the minus sign
        number = -number;
    }
    if(number < 10)                // treat 1 symbols like 2 symbols
    {
        number = 10;
    }

    long temp = 10;
    while(number >= temp)
    {
        positionsCount++;
        temp *= 10;
    }
//    if(number >= 10) positionsCount++;
//    if(number >= 100) positionsCount++;
//    if(number >= 1000) positionsCount++;
//    if(number >= 10000) positionsCount++;
//    if(number >= 100000) positionsCount++;
//    if(number >= 1000000) positionsCount++;

    for(byte i = positionsCount; i < sizeToTake; i++)
    {
        lcd.print(F(" "));
    }
}
void fillIntToTakeExactSpace(int number, byte sizeToTake)
{
	fillLongToTakeExactSpace((long) number, sizeToTake);
}
void fillLongToTakeExactSpace(long number, byte sizeToTake)
{
    byte temp = sizeToTake - 1;
    long bigNumber = 1;
    
    if(number < 0) 
    {
        temp--; // TODO: this is not tested for negative values!
        number = -number;
    }

    for(byte i = 0; i < temp; i++)
    {
        bigNumber*=10;
    }
    while(bigNumber > number && !(bigNumber == 1 && number == 0))
    {
        lcd.print(F(" "));
        bigNumber /=10;
    }
}
byte lengthOfLongNumber(long number)
{
    byte size = 1;
    if(number < 0)
    {
        number = -number; 
        size++;             // TODO: this is not tested for negative values!
    }
    long temp = 10;
    while(number >= temp)
    {
        size++;
        temp *= 10;
    }
  //  if(number >= 10) size++;
   // if(number >= 100) size++;
  //  if(number >= 1000) size++;
  //  if(number >= 10000) size++;
    return size;
}
void printEmptySpaces(int countSpaces)
{
    for(byte i = 0; i < countSpaces; i++)
        lcd.print(" ");
}
void displayFloatIntInExactSpace(long floatIntValue, byte spaceToTake, byte decimals)
{
    if (floatIntValue == INITIAL_DATA)
    {
        printEmptySpaces(spaceToTake - 2); // 2 is "--"
        lcd.print(F("--"));
    }
    else
    {
        float tempFloat = getFloatValueFromLong(floatIntValue);

        byte tempLength = 1 + lengthOfLongNumber(floatIntValue); // add the dot
//        Serial.print("tempLength = ");
//        Serial.println(tempLength);
//        Serial.print("floatIntValue = ");
//        Serial.println(floatIntValue);

        if(tempLength > spaceToTake)
        {
            spaceToTake = spaceToTake; // remove the dot
            decimals = 0;
            //lcd.print(F(" "));
           // fillIntToTakeExactSpace(floatIntValue / 10, spaceToTake);
        }

        if(decimals == 0 )
        {
            fillIntToTakeExactSpace(floatIntValue / 10, spaceToTake);
        }
        else
        {
            fillFloatAsIntToTakeExactSpace(floatIntValue, spaceToTake, decimals);
        }
        lcd.print(tempFloat, decimals);
    }
}
bool displayConfirm(const __FlashStringHelper* newMessage1, byte length1, const __FlashStringHelper* newMessage2, byte length2, bool defaultResult)
{
    lcd.LcdClearAfterRow(1);
    globalStates.forceDisplayRedraw = true;
    long timeOpened = millis(); // no need for myMillis()

    if(newMessage1 != NULL && length1 > 0)
    {
        lcd.gotoXY(SYMBOL_SIZE * ((DISPLAY_SYMBOLS_PER_ROW - length1) / 2), 1);
        lcd.print(newMessage1);
    }
    if(newMessage2 != NULL && length2 > 0)
    {
        lcd.gotoXY(SYMBOL_SIZE * ((DISPLAY_SYMBOLS_PER_ROW - length2) / 2), 2);
        lcd.print(newMessage2);
    }

    lcd.gotoXY(OFFSET_LEFT, 4);
    lcd.print(F("Y"));
    lcd.gotoXY(OFFSET_LEFT + 13 * SYMBOL_SIZE, 4);
    lcd.print(F("N"));

    delay(600); // wait enough to release the button

    bool result;
    while(true)
    {
        bool button4NowPressed = digitalRead(PIN_button4) == LOW;
        bool button3NowPressed = digitalRead(PIN_button3) == LOW;
        delay(30);
        
        if(button4NowPressed)
        {
            result = true;
            break;
        }
        else if(button3NowPressed)
        {
            result = false;
            break;
        }
        else if (millis() - timeOpened >= CONFIRM_WAIT_MAX_PERIOD_MS)
        {
            result = defaultResult;
            break;
        }
    }
    // clear the button presses
    while(digitalRead(PIN_button4) == LOW || digitalRead(PIN_button3) == LOW);
    
    return result;
}

bool displayConfirm(int number, byte length1, bool defaultResult)
{
    lcd.LcdClearAfterRow(1);
    globalStates.forceDisplayRedraw = true;
    long timeOpened = millis(); // no need for myMillis()

        lcd.gotoXY(SYMBOL_SIZE * ((DISPLAY_SYMBOLS_PER_ROW - length1) / 2), 1);
        lcd.print(number);


    lcd.gotoXY(OFFSET_LEFT, 4);
    lcd.print(F("Y"));
    lcd.gotoXY(OFFSET_LEFT + 13 * SYMBOL_SIZE, 4);
    lcd.print(F("N"));

    bool result;
    while(true)
    {
        bool button4NowPressed = digitalRead(PIN_button4) == LOW;
        bool button3NowPressed = digitalRead(PIN_button3) == LOW;
        delay(30);
        
        if(button4NowPressed)
        {
            result = true;
            break;
        }
        else if(button3NowPressed)
        {
            result = false;
            break;
        }
        else if (millis() - timeOpened >= CONFIRM_WAIT_MAX_PERIOD_MS)
        {
            result = defaultResult;
            break;
        }
    }
    // clear the button presses
    while(digitalRead(PIN_button4) == LOW || digitalRead(PIN_button3) == LOW);
}


// functions for the display header
void initTimer()
{
    byte lastSec = globalStates.lastSec;
    byte lastMin = globalStates.lastMin;
    byte lastHour = globalStates.lastHour;
    byte lastDay = globalStates.lastDay;

    lcd.gotoXY(OFFSET_LEFT, 0);
    if(lastHour < 10)
        lcd.print("0");
    lcd.print(lastHour);

    lcd.print(":");

    lcd.gotoXY(OFFSET_LEFT + 3 * SYMBOL_SIZE, 0);
    if(lastMin < 10)
        lcd.print("0");
    lcd.print(lastMin);

    lcd.print(":");

    lcd.gotoXY(OFFSET_LEFT + 6 * SYMBOL_SIZE, 0);
    if(lastSec < 10)
        lcd.print("0");
    lcd.print(lastSec);
}
void displayTimer()
{
    byte lastSec = globalStates.lastSec;
    byte lastMin = globalStates.lastMin;
    byte lastHour = globalStates.lastHour;
    byte lastDay = globalStates.lastDay;

    long currTime = myMillis();

    long x = currTime / 1000;
    byte newSec  = x % 60;
    x /= 60;
    byte newMin  = x % 60;
    x /= 60;
    byte newHour = x % 24;
    x = x / 24;
    byte newDay  = x;

    if(lastSec != newSec) // secs changed
    {
        lcd.gotoXY(OFFSET_LEFT + 6*SYMBOL_SIZE, 0);
        if(newSec < 10)
            lcd.print("0");
        lcd.print(newSec);
        lastSec = newSec;
    }
    if(lastMin != newMin) // mins changed
    {
        lcd.gotoXY(OFFSET_LEFT + 3*SYMBOL_SIZE, 0);
        if(newMin < 10)
            lcd.print("0");
        lcd.print(newMin);
        lastMin = newMin;
    }
    if(lastHour != newHour) // hours changed
    {
        lcd.gotoXY(OFFSET_LEFT , 0);
        if(newHour < 10)
            lcd.print("0");
        lcd.print(newHour);
        lastHour = newHour;
    }
  /*  if(lastDay != newDay) // days changed
    {
        lcd.gotoXY(OFFSET_LEFT + SYMBOL_SIZE, 0);
  //      lcd.print(newDay);
        lastDay = newDay;
    }
    */
}
void displayHeaderSleepState(bool isSleeping)
{
    // clear the position before the symbol
    lcd.gotoXY(OFFSET_LEFT + 12*SYMBOL_SIZE, 0);
    lcd.print(" ");
    if(isSleeping)
        lcd.print("S");
    else
        lcd.print(" ");
}
void displayHeaderWrongSensorData(bool isWrong)
{
    lcd.gotoXY(OFFSET_LEFT + 12*SYMBOL_SIZE, 0);
    if(isWrong)
    {
        lcd.print(F("Wr"));
    }
    else
    {
        if(globalButtons.pinInterruptFlag == InterruptStates_NormalWork) // is awake
        {
            lcd.print(F("  "));    // two spaces
        }
        else
        {
            lcd.print(F(" S"));
        }
    }
}
void displayHeaderVoltage()
{
    // TODO - may be it is better to read voltage more rarely
    if(myMillis() - globalStates.lastTimeBatteryCheckMs > 5 * 1000)
    {
        // read and calibrate the voltage value
        //Vcc vcc;
        //float voltage = vcc.Read_Volts();
        //voltage *= VOLTAGE_CORRECTION_COEFFICIENT;
//        float voltage = ReadVCCNew() * VOLTAGE_CORRECTION_COEFFICIENT / 1000;
        float voltage = ReadVCCNew();
        globalStates.lastTimeBatteryCheckMs = myMillis();

        if(voltage > 0)
        {
            // display volts on the screen
            lcd.gotoXY(OFFSET_LEFT + 9*SYMBOL_SIZE, 0);
            lcd.print(voltage, 1);
        }
    }
}

//  common general functions
void showCenteredMessage(const __FlashStringHelper* newMessage, byte length, int delayPeriodMs)
{
    lcd.LcdClearAfterRow(1);
    lcd.gotoXY(SYMBOL_SIZE * ((DISPLAY_SYMBOLS_PER_ROW - length) / 2), 2);
    lcd.print(newMessage);
    delay(delayPeriodMs);
    globalStates.forceDisplayRedraw = true;
}
void displayMessageBox(__FlashStringHelper* message)
{
    lcd.LcdClearAfterRow(1);
    lcd.gotoXY(OFFSET_LEFT, 2);
    lcd.print(message);
}

// Normal State
void displayNormalScreen(bool forceDisplayRedraw)
{
// xx.xC xxxxhPa
// xx.x% xxxx.xm
    byte startPos;

    if(forceDisplayRedraw)
    {
        lcd.LcdClearAfterRow(1);
    }

    // temp
    if(forceDisplayRedraw || statistics.getFlagBit(FLAG_INDEX_TEMPERATURE))
    {
        statistics.setFlagBit(FLAG_INDEX_TEMPERATURE, false);
        startPos = OFFSET_LEFT;
        lcd.gotoXY(startPos,1);
        
        displayFloatIntInExactSpace(statistics.temperature, 5);
        lcd.gotoXY(5 * SYMBOL_SIZE, 1);
        lcd.print(F("C"));
    }

    // preasure
    if(forceDisplayRedraw || statistics.getFlagBit(FLAG_INDEX_PRESSURE))
    {
        statistics.setFlagBit(FLAG_INDEX_PRESSURE, false);
        startPos = 7 *SYMBOL_SIZE, 1; // max size for temp is 6 bytes + 1 space;
        lcd.gotoXY(startPos, 1);
        displayFloatIntInExactSpace(statistics.pressure, 4, 0);
        lcd.print(F("hPa"));
    }

    // humidity
    if(forceDisplayRedraw || statistics.getFlagBit(FLAG_INDEX_HUMIDITY))
    {
        statistics.setFlagBit(FLAG_INDEX_HUMIDITY, false);
        lcd.gotoXY(OFFSET_LEFT, 2);
        displayFloatIntInExactSpace(statistics.humidity, 5);
        lcd.print(F("%"));
    }
        
    // altitute
    if(forceDisplayRedraw || statistics.getFlagBit(FLAG_INDEX_ALTITUTE))
    {
        statistics.setFlagBit(FLAG_INDEX_ALTITUTE, false);
        startPos = 7 * SYMBOL_SIZE, 1; // max size for temp is 6 bytes + 1 space;

        lcd.gotoXY(startPos, 2);
        displayFloatIntInExactSpace(statistics.altitute, 6);
        lcd.print(F("m"));
    }
}

// Tracking State
void displayTrackingScreen(bool forceDisplayRedraw)
{
    float tempFloat;
    byte tempByte;
    byte startPos;
    
    if(forceDisplayRedraw)
    {
        lcd.LcdClearAfterRow(1);
    }

    // last registered altitute
    if(forceDisplayRedraw || statistics.getFlagBit(FLAG_INDEX_LAST_REGISTERED_ALTITUTE))
    {
        statistics.setFlagBit(FLAG_INDEX_LAST_REGISTERED_ALTITUTE, false);
        lcd.gotoXY(OFFSET_LEFT + 3 * SYMBOL_SIZE, 1);
        displayFloatIntInExactSpace(statistics.lastRegisteredAltitute, 6, 1);
    }

    // temp denivelation
    if(forceDisplayRedraw || statistics.getFlagBit(FLAG_INDEX_TEMP_DENIVELATION_UP) || statistics.getFlagBit(FLAG_INDEX_TEMP_DENIVELATION_DOWN))
    {
        statistics.setFlagBit(FLAG_INDEX_TEMP_DENIVELATION_UP, false);
        statistics.setFlagBit(FLAG_INDEX_TEMP_DENIVELATION_DOWN, false);
        lcd.gotoXY(OFFSET_LEFT + 3 * SYMBOL_SIZE, 2);
        long tempDenivelation = statistics.upDownFlag ? statistics.tempDenivelationUp : statistics.tempDenivelationDown;
        //displayFloatIntInExactSpace(tempDenivelation, 6);
        fillFloatAsIntToTakeExactSpace(tempDenivelation, 5, 1); // plus sign later
        if(statistics.upDownFlag)
            lcd.print(F("+"));
        else
            lcd.print(F("-"));
        tempFloat = getFloatValueFromInt(tempDenivelation);
        lcd.print(tempFloat, 1);
        lcd.print(F("m"));
    }

    // altitute
    if(forceDisplayRedraw || statistics.getFlagBit(FLAG_INDEX_ALTITUTE))
    {
        statistics.setFlagBit(FLAG_INDEX_ALTITUTE, false);
//        lcd.gotoXY(OFFSET_LEFT + 2 * SYMBOL_SIZE, 3);
//        displayFloatIntInExactSpace(statistics.altitute, 7);
//        lcd.print(F("m"));

        lcd.gotoXY(OFFSET_LEFT, 3);
        displayFloatIntInExactSpace(statistics.altitute, 7);
        lcd.print(F("m"));
        lcd.gotoXY(OFFSET_LEFT + 7 * SYMBOL_SIZE, 3);
        displayFloatIntInExactSpace(statistics.altituteRaw, 7);
    }
    
    if(statistics.currentLapIndex >= 0)
    {
        LapStatistics* currentLap = &statistics.lapStatistics[statistics.currentLapIndex];

        // lap denivelation up
        if(forceDisplayRedraw || statistics.getFlagBit(FLAG_INDEX_LAP_DENIVELATION_UP))   // no need for separate flag, because they reflect the same
        {
            statistics.setFlagBit(FLAG_INDEX_LAP_DENIVELATION_UP, false);
            lcd.gotoXY(OFFSET_LEFT, 4);
            // todo
            //displayFloatIntInExactSpace(statistics.totalDenivelationUp, 6, 1);
            tempByte = currentLap->lapTotalDenivelationUp > 9999 ? 0 : 1; // if the value will exceed 999.9m, then do not show the point
            fillFloatAsIntToTakeExactSpace(currentLap->lapTotalDenivelationUp, 5, tempByte); // 4, 0);
            lcd.print(F("+"));
            tempFloat = getFloatValueFromLong(currentLap->lapTotalDenivelationUp);
            lcd.print(tempFloat, tempByte); // has to be 0
            lcd.print(F("m"));
        }
    
        // lap denivelation down
        if(forceDisplayRedraw || statistics.getFlagBit(FLAG_INDEX_LAP_DENIVELATION_DOWN))
        {
            statistics.setFlagBit(FLAG_INDEX_LAP_DENIVELATION_DOWN, false);
            lcd.gotoXY(OFFSET_LEFT + 7 * SYMBOL_SIZE, 4);
            //displayFloatIntInExactSpace(statistics.totalDenivelationDown, 6, 1);
            tempByte = currentLap->lapTotalDenivelationDown > 9999 ? 0 : 1; // if the value will exceed 999.9m, then do not show the point
            fillFloatAsIntToTakeExactSpace(currentLap->lapTotalDenivelationDown, 5, tempByte); // 4, 0);
            lcd.print(F("-"));
            tempFloat = getFloatValueFromLong(currentLap->lapTotalDenivelationDown);
            lcd.print(tempFloat, tempByte); // has to be 0
            lcd.print(F("m"));
        }
    }

    // total denivelation up
    if(forceDisplayRedraw || statistics.getFlagBit(FLAG_INDEX_TOTAL_DENIVELATION_UP))
    {
        statistics.setFlagBit(FLAG_INDEX_TOTAL_DENIVELATION_UP, false);
        lcd.gotoXY(OFFSET_LEFT, 5);
        //displayFloatIntInExactSpace(statistics.totalDenivelationUp, 6, 1);
        tempByte = statistics.totalDenivelationDown > 9999 ? 0 : 1; // if the value will exceed 999m, then do not show the point
        fillFloatAsIntToTakeExactSpace(statistics.totalDenivelationUp, 5, tempByte); // 4, 0);
        lcd.print(F("+"));
        tempFloat = getFloatValueFromLong(statistics.totalDenivelationUp);
        lcd.print(tempFloat, tempByte); // has to be 0
        lcd.print(F("m"));
    }

    // total denivelation down
    if(forceDisplayRedraw || statistics.getFlagBit(FLAG_INDEX_TOTAL_DENIVELATION_DOWN))
    {
        statistics.setFlagBit(FLAG_INDEX_TOTAL_DENIVELATION_DOWN, false);
        lcd.gotoXY(OFFSET_LEFT + 7 * SYMBOL_SIZE, 5);
        //displayFloatIntInExactSpace(statistics.totalDenivelationDown, 6, 1);
        tempByte = statistics.totalDenivelationDown > 9999 ? 0 : 1; // if the value will exceed 999m, then do not show the point
        fillFloatAsIntToTakeExactSpace(statistics.totalDenivelationDown, 5, tempByte); // 4, 0);
        lcd.print(F("-"));
        tempFloat = getFloatValueFromLong(statistics.totalDenivelationDown);
        lcd.print(tempFloat,  tempByte);  
        lcd.print(F("m"));
    }
}

// Summary screen 1
void displaySummaryScreen1(bool forceDisplayRedraw)
{
    float tempFloat;
    byte OFFSET_SYMBOLS_SECOND_COLUMN = 5 * SYMBOL_SIZE;
    byte OFFSET_SYMBOLS_THIRD_COLUMN = 10 * SYMBOL_SIZE;

    if(forceDisplayRedraw)    
    {
        lcd.LcdClearAfterRow(1);
        lcd.gotoXY(OFFSET_LEFT + OFFSET_SYMBOLS_SECOND_COLUMN + SYMBOL_SIZE, 1);
        lcd.print(F("min"));

        lcd.gotoXY(OFFSET_LEFT+ OFFSET_SYMBOLS_THIRD_COLUMN + SYMBOL_SIZE, 1);
        lcd.print(F("max"));


        lcd.gotoXY(OFFSET_LEFT, 2);
        lcd.print(F("temp"));
        
        lcd.gotoXY(OFFSET_LEFT + OFFSET_SYMBOLS_SECOND_COLUMN, 2);
        displayFloatIntInExactSpace(statistics.minTemperature, 4);
        
        lcd.gotoXY(OFFSET_LEFT + OFFSET_SYMBOLS_THIRD_COLUMN, 2);
        displayFloatIntInExactSpace(statistics.maxTemperature, 4);

        lcd.gotoXY(OFFSET_LEFT, 3);
        lcd.print(F("humi"));
        
        lcd.gotoXY(OFFSET_LEFT + OFFSET_SYMBOLS_SECOND_COLUMN, 3);
        displayFloatIntInExactSpace(statistics.minHumidity, 4);
        
        lcd.gotoXY(OFFSET_LEFT + OFFSET_SYMBOLS_THIRD_COLUMN, 3);
        displayFloatIntInExactSpace(statistics.maxHumidity, 4);
        
        lcd.gotoXY(OFFSET_LEFT, 4);
        lcd.print(F("pres"));
        
        lcd.gotoXY(OFFSET_LEFT + OFFSET_SYMBOLS_SECOND_COLUMN, 4);
        displayFloatIntInExactSpace(statistics.minPressure, 4, 0);
        
        lcd.gotoXY(OFFSET_LEFT + OFFSET_SYMBOLS_THIRD_COLUMN, 4);
        displayFloatIntInExactSpace(statistics.maxPressure, 4, 0);

        lcd.gotoXY(OFFSET_LEFT, 5);
        lcd.print(F("alti"));
        
        lcd.gotoXY(OFFSET_LEFT + OFFSET_SYMBOLS_SECOND_COLUMN, 5);
        displayFloatIntInExactSpace(statistics.minAltitute, 4, 0);
        
        lcd.gotoXY(OFFSET_LEFT + OFFSET_SYMBOLS_THIRD_COLUMN, 5);
        displayFloatIntInExactSpace(statistics.maxAltitute, 4, 0);
        //fillIntToTakeExactSpace(statistics.maxAltitute / 10, 4); // we devide by 10, because we ignore the last number
        //tempFloat = getFloatValueFromInt(statistics.maxAltitute);
        //lcd.print(tempFloat, 0);
    }

}

// Summary screen 2
void displaySummaryScreen2(bool forceDisplayRedraw)
{    
    float tempFloat;
    byte OFFSET_SYMBOLS_SECOND_COLUMN = 9 * SYMBOL_SIZE;

    if(forceDisplayRedraw)    
    {
        lcd.LcdClearAfterRow(1);
        lcd.gotoXY(OFFSET_LEFT, 1);
        lcd.print(F("Total(+)"));

        lcd.gotoXY(OFFSET_LEFT+ OFFSET_SYMBOLS_SECOND_COLUMN, 1);
        displayFloatIntInExactSpace(statistics.totalDenivelationUp, 4, 0);
//        fillIntToTakeExactSpace(statistics.totalDenivelationUp / 10, 4); // we devide by 10, because we ignore the last number
//        tempFloat = getFloatValueFromInt(statistics.totalDenivelationUp);
//        lcd.print(tempFloat, 0);

        lcd.gotoXY(OFFSET_LEFT, 2);
        lcd.print(F("Total(-)"));

        lcd.gotoXY(OFFSET_LEFT+ OFFSET_SYMBOLS_SECOND_COLUMN, 2);
        displayFloatIntInExactSpace(statistics.totalDenivelationDown, 4, 0);
//        fillIntToTakeExactSpace(statistics.totalDenivelationDown / 10, 4); // we devide by 10, because we ignore the last number
//        tempFloat = getFloatValueFromInt(statistics.totalDenivelationDown);
//        lcd.print(tempFloat, 0);


        lcd.gotoXY(OFFSET_LEFT, 3);
        lcd.print(F("Temp(+)"));

        lcd.gotoXY(OFFSET_LEFT+ OFFSET_SYMBOLS_SECOND_COLUMN, 3);
        displayFloatIntInExactSpace(statistics.maxTempDenivelationUp, 4, 0);
        //fillIntToTakeExactSpace(statistics.maxTempDenivelationUp / 10, 4); // we devide by 10, because we ignore the last number
        //tempFloat = getFloatValueFromInt(statistics.maxTempDenivelationUp);
        //lcd.print(tempFloat, 0);


        lcd.gotoXY(OFFSET_LEFT, 4);
        lcd.print(F("Temp(-)"));

        lcd.gotoXY(OFFSET_LEFT+ OFFSET_SYMBOLS_SECOND_COLUMN, 4);
        displayFloatIntInExactSpace(statistics.maxTempDenivelationDown, 4, 0);
        //fillIntToTakeExactSpace(statistics.maxTempDenivelationDown / 10, 4); // we devide by 10, because we ignore the last number
        //tempFloat = getFloatValueFromInt(statistics.maxTempDenivelationDown);
        //lcd.print(tempFloat, 0);

    }
}

// Summary screen 3
void displaySummaryScreen3(bool forceDisplayRedraw)
{    
    float tempFloat;
    byte OFFSET_SYMBOLS_SECOND_COLUMN = 9 * SYMBOL_SIZE;

    if(forceDisplayRedraw)    
    {
        lcd.LcdClearAfterRow(1);
        lcd.gotoXY(OFFSET_LEFT, 1);
        lcd.print(F("EEPROMCnt"));

        lcd.gotoXY(OFFSET_LEFT + OFFSET_SYMBOLS_SECOND_COLUMN, 1);
        fillLongToTakeExactSpace(statistics.eepromWriteCounter, 5);
        lcd.print(statistics.eepromWriteCounter);
    }
}

// Summary screen 4
void displaySummaryScreen4(bool forceDisplayRedraw)
{    
    // 33% display min/max value, 67% display the chart
    long blinkPeriod = millis() % ( 3 * SUMMARY_BLINK_PERIOD); // no need for myMillis()
    bool blinkFlag = blinkPeriod > SUMMARY_BLINK_PERIOD;
    if(forceDisplayRedraw || _blinkFlag != blinkFlag)    
    {
        _blinkFlag = blinkFlag;

        unsigned int minAltitute = 0xFFFF;
        unsigned int maxAltitute = 0;

        char startPos = globalsPressureLogger.getFirstPosition();
  //      Sprint("startPos =  ");
  //      Sprintln(startPos);
        for(int i = 0; i < globalsPressureLogger.count; i++)
        {
            int x = globalsPressureLogger.data[(startPos + i) % PRESSURE_LOGGER_ARRAY_SIZE];
//        Sprint("x =  ");
//        Sprintln(x);
            minAltitute = min(minAltitute, x);
            maxAltitute = max(maxAltitute, x);
        }
        float coeff = 1;
        if(maxAltitute != minAltitute)
        {
            coeff = (float) (48 - 8 - 1) / (float)(maxAltitute - minAltitute);
        }
//        Sprint("minAltitute =  ");
//        Sprintln(minAltitute);
//        Sprint("maxAltitute =  ");
//        Sprintln(maxAltitute);
//        Sprint("coeff =  ");
//        Sprintln(coeff);
//        Sprint("count =  ");
//        Sprintln(globalsPressureLogger.count);

        if(_blinkFlag)
        {
    //        Sprint("y =  ");
            lcd.LcdClearAfterRow(1);
            for(int i = 0; i < globalsPressureLogger.count; i++)
            {
                int x = globalsPressureLogger.data[(startPos + i) % PRESSURE_LOGGER_ARRAY_SIZE];
                byte yValue = (byte)(coeff * (x - minAltitute));
                byte yRow = yValue / 8;
                byte bValue = 1 << (7 - (yValue % 8));
                lcd.gotoXY(i,  5 - yRow);
                //lcd.print(".");
                lcd.LcdWrite(LCD_DATA, bValue);
    //            Sprint(yValue);
    //            Sprint(" ");
            }
        }
        else
        {
            lcd.LcdClearAfterRow(1);
            lcd.gotoXY(OFFSET_LEFT,  4);
            lcd.print(F("min: "));
            lcd.print(getFloatValueFromLong(minAltitute), 1);
            lcd.print(F("m"));

            lcd.gotoXY(OFFSET_LEFT,  1);
            lcd.print(F("max: "));
            lcd.print(getFloatValueFromLong(maxAltitute), 1);
            lcd.print(F("m"));

        }
//        Sprintln(" ");

        //lcd.gotoXY(OFFSET_LEFT, 1);
     //   lcd.print(F("EEPROMCnt"));

     //   lcd.gotoXY(OFFSET_LEFT + OFFSET_SYMBOLS_SECOND_COLUMN, 1);
     //   fillIntToTakeExactSpace(statistics.eepromWriteCounter, 5);
     //   lcd.print(statistics.eepromWriteCounter);
    }
}

void displaySettingsGeneral(bool forceDisplayRedraw, int currentValue, const __FlashStringHelper * text1, const __FlashStringHelper * text2, byte arr[], byte arraySize, byte rowNumber)
{
    if(forceDisplayRedraw)    
    {
        lcd.LcdClearAfterRow(1);
        lcd.gotoXY(OFFSET_LEFT, 1);
        lcd.print(text1);

        if(text2 != NULL)
        {
            lcd.gotoXY(OFFSET_LEFT, 2);
            lcd.print(text2);
        }
    }

    long blinkPeriod = millis() % ( 2 * SETTINGS_BLINK_PERIOD); // no need for myMillis()
    bool blinkFlag = blinkPeriod > SETTINGS_BLINK_PERIOD;
    if(forceDisplayRedraw || _blinkFlag != blinkFlag)    
    {
        _blinkFlag = blinkFlag;

//        byte settings_values_contrast[]                   = SETTINGS_ARRAY_CONTRAST

///        Serial.print("size = ");
//        Serial.println(arraySize);

        byte totalLength = 0;
        byte cursorPos = OFFSET_LEFT;
        //byte rowNumber = 2;
        for(byte i=0; i < arraySize; i++)
        {
            lcd.gotoXY(cursorPos, rowNumber);
            byte tempLength = lengthOfLongNumber(arr[i]);

//        Serial.print("arr[i] = ");
//        Serial.println(arr[i]);

            if(blinkFlag == true || arr[i] != currentValue)
            {
                lcd.print(arr[i]);
            }
            else
            {
                printEmptySpaces(tempLength);
            }

            cursorPos+= (tempLength + 1) * SYMBOL_SIZE;

            if(cursorPos + (tempLength + 1) * SYMBOL_SIZE >  DISPLAY_SYMBOLS_PER_ROW * SYMBOL_SIZE) // try to predict next number size
            {
                cursorPos = OFFSET_LEFT;
                rowNumber++;
            }
        }
    }
}

// Settings1 - SetContrast
void displaySettingsScreenSetContrast(bool forceDisplayRedraw, byte currentValue)
{
    byte settings_values_contrast[]                   = SETTINGS_ARRAY_CONTRAST
    displaySettingsGeneral(forceDisplayRedraw, currentValue, F("Contrast:"), NULL, settings_values_contrast,  ARRAY_SIZE(settings_values_contrast), 2);
    /*
    if(forceDisplayRedraw)    
    {
        lcd.LcdClearAfterRow(1);
        lcd.gotoXY(OFFSET_LEFT, 1);
        lcd.print(F("Contrast:"));
    }

    long blinkPeriod = millis() % ( 2 * SETTINGS_BLINK_PERIOD); // no need for myMillis()
    bool blinkFlag = blinkPeriod > SETTINGS_BLINK_PERIOD;
    if(forceDisplayRedraw || _blinkFlag != blinkFlag)    
    {
        _blinkFlag = blinkFlag;

        byte settings_values_contrast[]                   = SETTINGS_ARRAY_CONTRAST

        byte totalLength = 0;
        byte cursorPos = OFFSET_LEFT;
        byte rowNumber = 2;
        for(byte i=0; i < ARRAY_SIZE(settings_values_contrast); i++)
        {
            lcd.gotoXY(cursorPos, rowNumber);
            byte tempLength = lengthOfIntNumber(settings_values_contrast[i]);
            if(blinkFlag == true || settings_values_contrast[i] != currentValue)
            {
                lcd.print(settings_values_contrast[i]);
            }
            else
            {
                printEmptySpaces(tempLength);
            }

            cursorPos+= (tempLength + 1) * SYMBOL_SIZE;

            if(cursorPos + (tempLength + 1) * SYMBOL_SIZE >  DISPLAY_SYMBOLS_PER_ROW * SYMBOL_SIZE) // try to predict next number size
            {
                cursorPos = OFFSET_LEFT;
                rowNumber++;
            }
        }
    }
    */
}

// Settings2 - TurnOffDisplayAfter
void displaySettingsScreenTurnOffDisplayAfter(bool forceDisplayRedraw, byte currentValue)
{
    byte settings_values_turn_off_display_after_min[]   = SETTINGS_ARRAY_TURN_OFF_DISPLAY_AFTER_MIN
    displaySettingsGeneral(forceDisplayRedraw, currentValue, F("OFF disp after"), NULL, settings_values_turn_off_display_after_min,  ARRAY_SIZE(settings_values_turn_off_display_after_min), 2);
/*
    if(forceDisplayRedraw)    
    {
        lcd.LcdClearAfterRow(1);

        lcd.gotoXY(OFFSET_LEFT, 1);
        lcd.print(F("OFF disp after"));
    }

    long blinkPeriod = millis() % ( 2 * SETTINGS_BLINK_PERIOD); // no need for myMillis()
    bool blinkFlag = blinkPeriod > SETTINGS_BLINK_PERIOD;
    if(forceDisplayRedraw || _blinkFlag != blinkFlag)    
    {
        _blinkFlag = blinkFlag;

        byte settings_values_turn_off_display_after_min[]   = SETTINGS_ARRAY_TURN_OFF_DISPLAY_AFTER_MIN

        byte totalLength = 0;
        byte cursorPos = OFFSET_LEFT;
        byte rowNumber = 2;
        for(byte i=0; i < ARRAY_SIZE(settings_values_turn_off_display_after_min); i++)
        {
            lcd.gotoXY(cursorPos, rowNumber);
            byte tempLength = lengthOfIntNumber(settings_values_turn_off_display_after_min[i]);
            if(blinkFlag == true || settings_values_turn_off_display_after_min[i] != currentValue)
            {
                lcd.print(settings_values_turn_off_display_after_min[i]);
            }
            else
            {
                printEmptySpaces(tempLength);
            }
            cursorPos+= (tempLength + 1) * SYMBOL_SIZE;

            if(cursorPos + (tempLength + 1) * SYMBOL_SIZE >  DISPLAY_SYMBOLS_PER_ROW * SYMBOL_SIZE) // try to predict next number size
            {
                cursorPos = OFFSET_LEFT;
                rowNumber++;
            }
        }
    }
*/
}

// Settings3 - Read Sensor Period screen
void displaySettingsScreenReadSensorPeriod(bool forceDisplayRedraw, byte currentValue)
{    
    byte settings_valuessensor_period[] = SETTINGS_ARRAY_READ_SENSOR_PERIOD_S
    displaySettingsGeneral(forceDisplayRedraw, currentValue, F("Read Sensor(s)"), NULL, settings_valuessensor_period,  ARRAY_SIZE(settings_valuessensor_period), 2);
    /*
    if(forceDisplayRedraw)    
    {
        lcd.LcdClearAfterRow(1);
        lcd.gotoXY(OFFSET_LEFT, 1);
        lcd.print(F("Read Sensor(s)"));
    }

    long blinkPeriod = millis() % ( 2 * SETTINGS_BLINK_PERIOD); // no need for myMillis()
    bool blinkFlag = blinkPeriod > SETTINGS_BLINK_PERIOD;
    if(forceDisplayRedraw || _blinkFlag != blinkFlag)    
    {
        _blinkFlag = blinkFlag;

        byte settings_valuessensor_period[] = SETTINGS_ARRAY_READ_SENSOR_PERIOD_S

        byte totalLength = 0;
        byte cursorPos = OFFSET_LEFT;
        byte rowNumber = 2;
        for(byte i=0; i < ARRAY_SIZE(settings_valuessensor_period); i++)
        {
            lcd.gotoXY(cursorPos, rowNumber);
            byte tempLength = lengthOfIntNumber((int) SETTINGS_ARRAY_READ_SENSOR_PERIOD_MULTIPLYER * settings_valuessensor_period[i]);
            if(blinkFlag == true || settings_valuessensor_period[i] != currentValue)
            {
                lcd.print((int) SETTINGS_ARRAY_READ_SENSOR_PERIOD_MULTIPLYER * settings_valuessensor_period[i]);
            }
            else
            {
                printEmptySpaces(tempLength);
            }

            cursorPos+= (tempLength + 1) * SYMBOL_SIZE;

            if(cursorPos + (tempLength + 1) * SYMBOL_SIZE > DISPLAY_SYMBOLS_PER_ROW * SYMBOL_SIZE) // try to predict next number size
            {
                cursorPos = OFFSET_LEFT;
                rowNumber++;
            }
        }
    }
    */
}

// Settings4 - Reset all
void displaySettingsScreenResetAll(bool forceDisplayRedraw, unsigned long resetActivationTime)
{
    if(forceDisplayRedraw)
    {
        lcd.LcdClearAfterRow(1);
        lcd.gotoXY(OFFSET_LEFT, 1);
        lcd.print(F("clear"));
        lcd.gotoXY(OFFSET_LEFT, 2);
        lcd.print(F("statistics?"));
    }
    if(resetActivationTime > 0)
    {
        lcd.gotoXY(OFFSET_LEFT, 3);
        lcd.print(F("reset in: "));
        byte remainingSecs = 5 - (millis() - resetActivationTime) / 1000;
        lcd.print(remainingSecs);
        lcd.print(F("s"));
    }
}


// Settings5 - Interpolate Altitute
void displaySettingsScreenInterpolateAltitute(bool forceDisplayRedraw, int currentValue)
{
//    Serial.print("new alt = ");
//    Serial.println(currentValue);
    byte settings_values_interpolate_altitute[] = SETTINGS_ARRAY_INTERPOLATE_ALTITUTE
    displaySettingsGeneral(forceDisplayRedraw, currentValue, F("Interpolate"), F("altitute?"), settings_values_interpolate_altitute,  ARRAY_SIZE(settings_values_interpolate_altitute), 3);
    /*
    if(forceDisplayRedraw)    
    {
        lcd.LcdClearAfterRow(1);
        lcd.gotoXY(OFFSET_LEFT, 1);
        lcd.print(F("Interpolate"));
        lcd.gotoXY(OFFSET_LEFT, 2);
        lcd.print(F("altitute?"));
    }

    long blinkPeriod = millis() % ( 2 * SETTINGS_BLINK_PERIOD); // no need for myMillis()
    bool blinkFlag = blinkPeriod > SETTINGS_BLINK_PERIOD;
    if(forceDisplayRedraw || _blinkFlag != blinkFlag)    
    {
        _blinkFlag = blinkFlag;

        byte settings_values_interpolate_altitute[] = SETTINGS_ARRAY_INTERPOLATE_ALTITUTE

        byte totalLength = 0;
        byte cursorPos = OFFSET_LEFT;
        byte rowNumber = 3;
        for(byte i=0; i < ARRAY_SIZE(settings_values_interpolate_altitute); i++)
        {
            lcd.gotoXY(cursorPos, rowNumber);
            byte tempLength = lengthOfIntNumber((int) SETTINGS_ARRAY_READ_SENSOR_PERIOD_MULTIPLYER * settings_values_interpolate_altitute[i]);
            if(blinkFlag == true || settings_values_interpolate_altitute[i] != currentValue)
            {
                lcd.print((int) SETTINGS_ARRAY_READ_SENSOR_PERIOD_MULTIPLYER * settings_values_interpolate_altitute[i]);
            }
            else
            {
                printEmptySpaces(tempLength);
            }

            cursorPos+= (tempLength + 1) * SYMBOL_SIZE;

            if(cursorPos + (tempLength + 1) * SYMBOL_SIZE > DISPLAY_SYMBOLS_PER_ROW * SYMBOL_SIZE) // try to predict next number size
            {
                cursorPos = OFFSET_LEFT;
                rowNumber++;
            }
        }
    }
    */
}

// SettingsX - Interpolate Altitute
void displaySettingsScreenCalibrateAltitute (bool forceDisplayRedraw, int currentValue, bool flagCalibrationDirection)
{
    if(forceDisplayRedraw)    
    {
        lcd.LcdClearAfterRow(1);
        lcd.gotoXY(OFFSET_LEFT, 1);
        lcd.print(F("Calibrate(m)"));
//        lcd.gotoXY(OFFSET_LEFT, 2);
//        lcd.print(F("altitute?"));
    }

    long blinkPeriod = millis() % ( 2 * SETTINGS_BLINK_PERIOD); // no need for myMillis()
    bool blinkFlag = blinkPeriod > SETTINGS_BLINK_PERIOD;
    if(forceDisplayRedraw || _blinkFlag != blinkFlag)    
    {

   //     Sprint("new value: ");
//       Sprintln(currentValue);
        _blinkFlag = blinkFlag;

        long currentHeight = statistics.altituteRaw / 10 + currentValue;
        lcd.gotoXY(OFFSET_LEFT, 2);
        fillIntToTakeExactSpace(currentHeight, 4);
        lcd.print(currentHeight);
        lcd.print(F("m"));

        lcd.gotoXY(OFFSET_LEFT, 3);
        fillIntToTakeExactSpace(currentValue, 4);
        lcd.print(currentValue);

        lcd.gotoXY(OFFSET_LEFT, 4);
        flagCalibrationDirection ? lcd.print("+") : lcd.print("-");
    }
}


// Settings5 - Backlight
void displaySettingsBacklight(bool forceDisplayRedraw, byte currentValue)
{
    byte settings_backlight[] = SETTINGS_ARRAY_BACKLIGHT_PERCENTAGE
    displaySettingsGeneral(forceDisplayRedraw, currentValue, F("Backlight %"), NULL, settings_backlight,  ARRAY_SIZE(settings_backlight), 2);
}


void displayLapSummaryScreen(bool forceDisplayRedraw, byte currentLapToView, byte totalLaps)
{
    byte OFFSET_SYMBOLS_SECOND_COLUMN = 9 * SYMBOL_SIZE;

    if(forceDisplayRedraw && currentLapToView >= 0)    
    {
        LapStatistics* currentLap = &statistics.lapStatistics[currentLapToView];

        lcd.LcdClearAfterRow(1);
        lcd.gotoXY(OFFSET_LEFT, 1);
        lcd.print(F("Lap: "));
        lcd.print(currentLapToView + 1);
        lcd.print(F("("));
        lcd.print(totalLaps);
        lcd.print(F(")"));

        lcd.gotoXY(OFFSET_LEFT, 2);
        lcd.print(F("Total(+)"));
        lcd.gotoXY(OFFSET_LEFT+ OFFSET_SYMBOLS_SECOND_COLUMN, 2);
        displayFloatIntInExactSpace(currentLap->lapTotalDenivelationUp, 5, 0);

        lcd.gotoXY(OFFSET_LEFT, 3);
        lcd.print(F("Total(-)"));
        lcd.gotoXY(OFFSET_LEFT+ OFFSET_SYMBOLS_SECOND_COLUMN, 3);
        displayFloatIntInExactSpace(currentLap->lapTotalDenivelationDown, 5, 0);

        lcd.gotoXY(OFFSET_LEFT, 4);
        lcd.print(F("Max"));
        lcd.gotoXY(OFFSET_LEFT+ OFFSET_SYMBOLS_SECOND_COLUMN, 4);
        displayFloatIntInExactSpace(currentLap->lapMaxAltitute, 5, 0);
        
        lcd.gotoXY(OFFSET_LEFT, 5);
        lcd.print(F("Min"));
        lcd.gotoXY(OFFSET_LEFT+ OFFSET_SYMBOLS_SECOND_COLUMN, 5);
        displayFloatIntInExactSpace(currentLap->lapMinAltitute, 5, 0);
    }
}