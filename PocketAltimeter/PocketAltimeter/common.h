#ifndef COMMON_H
#define COMMON_H

#include <Arduino.h>
#include <avr/sleep.h>

long myMillis();

void freezeMillis(long periodMs);
void defrostMillis();

void increaseDiscrepancy(long periodMs);

// move those two functions to common place
float getFloatValueFromInt(int value);
float getFloatValueFromLong(long value);



// Battery measure
// int getBatteryLevel ();
// long readVcc();

#endif // COMMON_H
